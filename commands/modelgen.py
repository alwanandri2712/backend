

from flask import current_app
from flask_script import Command
import subprocess

class GenerateModel(Command):
    """ Initialize the database."""

    def run(self):
        print("Generating model, please wait...")
        subprocess.call(["sh","./modelgen.sh"])
        print("Model generated.")