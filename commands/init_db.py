# This file defines command line commands for manage.py
#
# Copyright 2014 SolidBuilds.com. All rights reserved
#
# Authors: Ling Thio <ling.thio@gmail.com>

import datetime
import uuid

from flask import current_app
from flask_script import Command

from app import db
from models.user_models import User, Role
#from app.models.entities import UserProfile

class InitDbCommand(Command):
    """ Initialize the database."""

    def run(self):
        init_db()

def init_db():
    """ Initialize the database."""
    db.drop_all()
    db.create_all()
    create_users()


def create_users():
    """ Create users """

    # Create all tables
    db.create_all()

    # Adding roles
    admin_role = find_or_create_role('admin', u'Admin')

    # Add users
    user = find_or_create_user(u'Admin', u'Example','admin', u'admin@example.com', 'Password1', admin_role)
    user = find_or_create_user(u'Member', u'Example','member', u'member@example.com', 'Password1')

    # Save to DB
    db.session.commit()


def find_or_create_role(name, label):
    """ Find existing role or create new role """
    role = db.session.query(Role).filter(Role.name == name).first()
    if not role:
        role = Role(name=name, label=label,deleted=False)
        db.session.add(role)
    return role


def find_or_create_user(first_name, last_name,username, email, password, role=None):
    """ Find existing user or create new user """
    user = db.session.query(User).filter(User.username == username).first()
    if not user:
        user = User(email=email,
                    username=username,
                    first_name=first_name,
                    last_name=last_name,
                    password=current_app.user_manager.hash_password(password),
                    is_active=True,
                    deleted=False)

        if role:
           user.roles.append(role)
        db.session.add(user)
    return user



