
from flask_script import Command
from gevent.pywsgi import WSGIServer
from app import create_app
from routes import Router


class ProductionServer(Command):

    def run(self):
        print(self.get_options())
        app = create_app()
        router = Router(app)
        router.register_view()
        http_server = WSGIServer(('', 5000), app)
        http_server.serve_forever()