import os

# *****************************
# Environment specific settings
# *****************************

# DO NOT use "DEBUG = True" in production environments
DEBUG = True
MODE = 'development' #production

# DO NOT use Unsecure Secrets in production environments
# Generate a safe one with:
#     python -c "from __future__ import print_function; import string; import random; print(''.join([random.choice(string.ascii_letters + string.digits + string.punctuation) for x in range(24)]));"
SECRET_KEY = 'This is an UNSECURE Secret. CHANGE THIS for production environments.'

DB_PASSWORD = "k0pl4k"
DB_USERNAME = "postgres"
DB_NAME = "pdam"
DB_HOST ="52.230.20.14"
DB_PORT = "5432"

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}/{3}'.format(DB_USERNAME,DB_PASSWORD,DB_HOST,DB_NAME)
SQLALCHEMY_TRACK_MODIFICATIONS = False    # Avoids a SQLAlchemy Warning

JWT_SECRET_KEY = SECRET_KEY
JWT_ACCESS_TOKEN_EXPIRES = 3600*60*60
JWT_TOKEN_LOCATION = ['headers','query_string']
SEND_FILE_MAX_AGE_DEFAULT = 0
USER_ENABLE_EMAIL = False
# Flask-Mail settings
# For smtp.gmail.com to work, you MUST set "Allow less secure apps" to ON in Google Accounts.
# Change it in https://myaccount.google.com/security#connectedapps (near the bottom).
# MAIL_SERVER = 'smtp.gmail.com'
# MAIL_PORT = 587
# MAIL_USE_SSL = False
# MAIL_USE_TLS = True
# MAIL_USERNAME = 'm.ar13f@gmail'
# MAIL_PASSWORD = 'Kakan2021'
# MAIL_DEFAULT_SENDER = '"You" <m.ar13f@gmail>'
# ADMINS = [
#     '"Admin One" <m.ar13f@gmail>',
#     ]
