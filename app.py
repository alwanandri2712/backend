# __init__.py is a special Python file that allows a directory to become
# a Python package so it can be accessed using the 'import' statement.

# __init__.py is a special Python file that allows a directory to become
# a Python package so it can be accessed using the 'import' statement.
import os

import click
from flask import Flask
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_user import UserManager
from flask_wtf.csrf import CSRFProtect
from flask_cors import CORS
#import wtforms_json


import logging
import logging.handlers

from flask_jwt_extended import (
    JWTManager)

# Instantiate Flask extensions
db = SQLAlchemy()
csrf_protect = CSRFProtect()
mail = Mail()
migrate = Migrate()


def create_app(extra_config_settings={}):
    """Create a Flask applicaction.
    """
    # Instantiate Flask
    app = Flask(__name__)

    # Load App Config settings
    # Load common settings from 'settings.py' file
    app.config.from_object('settings')
    # Load local settings from 'local_settings.py'
    app.config.from_object('local_settings')
    # Load extra config settings from 'extra_config_settings' param
    app.config.update(extra_config_settings)

    # Setup Flask-Extensions -- do this _after_ app config has been loaded

    # Setup Flask-SQLAlchemy
    db.init_app(app)
    # db.drop_all(app=app)
    # db.create_all(app=app)

    # Setup Flask-Migrate
    migrate.init_app(app, db)

    # Setup Flask-Mail
    mail.init_app(app)

    # Setup WTForms CSRFProtect
    csrf_protect.init_app(app)

    #enable cors
    CORS(app)

    #wtforms_json.init()

    # Setup an error-logger to send emails to app.config.ADMINS
    #init_email_error_handler(app)
    init_file_error_handler(app)

    # Setup Flask-User to handle user account related forms
    from models.entities import User

    user_manager = UserManager(app, db, User)

    # jwt setup
    jwt = JWTManager(app)

    @jwt.user_claims_loader
    def add_claims_to_access_token(identity):
        user = User()
        user = db.session.query(User).filter(email=identity)
        claimRoles = []
        if user is not None:
            roles = user.roles;
            for role in roles:
                claimRoles.append(role.name)
        return {'roles': claimRoles}

    return app


def init_file_error_handler(app):
    handler = logging.handlers.RotatingFileHandler(
        'log.txt',
        maxBytes=1024 * 1024)
    app.logger.setLevel(logging.ERROR)
    app.logger.addHandler(handler)
    app.logger.disabled = True

    log = logging.getLogger('werkzeug')
    log.disabled = True
    log.setLevel(logging.ERROR)

    os.environ['WERKZEUG_RUN_MAIN'] = 'true'

    def secho(text, file=None, nl=None, err=None, color=None, **styles):
        pass

    def echo(text, file=None, nl=None, err=None, color=None, **styles):
        pass

    click.echo = echo
    click.secho = secho

def init_email_error_handler(app):
    """
    Initialize a logger to send emails on error-level messages.
    Unhandled exceptions will now send an email message to app.config.ADMINS.
    """
    if app.debug: return  # Do not send error emails while developing

    # Retrieve email settings from app.config
    host = app.config['MAIL_SERVER']
    port = app.config['MAIL_PORT']
    from_addr = app.config['MAIL_DEFAULT_SENDER']
    username = app.config['MAIL_USERNAME']
    password = app.config['MAIL_PASSWORD']
    secure = () if app.config.get('MAIL_USE_TLS') else None

    # Retrieve app settings from app.config
    to_addr_list = app.config['ADMINS']
    subject = app.config.get('APP_SYSTEM_ERROR_SUBJECT_LINE', 'System Error')

    # Setup an SMTP mail handler for error-level messages
    import logging
    from logging.handlers import SMTPHandler

    mail_handler = SMTPHandler(
        mailhost=(host, port),  # Mail host and port
        fromaddr=from_addr,  # From address
        toaddrs=to_addr_list,  # To address
        subject=subject,  # Subject line
        credentials=(username, password),  # Credentials
        secure=secure,
    )
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)
    # Log errors using: app.logger.error('Some error message')


if __name__ == '__main__':
    import models.entities as model
    q = db.session.query(model.User)
    q.all()
    q.one()





