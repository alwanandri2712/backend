# uvicorn main:app --reload
# http://localhost:8000/docs
import base64
from datetime import datetime

from docx.shared import Cm
from docxtpl import DocxTemplate, InlineImage
import qrcode
from app.reporting.cakum.get_image_size import get_image_size

import app.helpers.general as helper

from app import db

from app.reporting.setting import app_dir,logger

path = app_dir + "/reporting/cakum"
result_path = path + "/result"
qrpath = 'http://207.148.116.191/#report/SCK/'


#@app.get("/doc_cakum_sprin/{no_sprin}")
def createdoc_cakum_sprin(no_sprin:str):
    doc = DocxTemplate(path + "/CAKUM_SPRIN.docx")
    doc_json = {}
    context = {
    'no_sprin': '',
    'permohonan_wilayah': '..........................',
    'permohonan_no': '........................',
    'permohonan_tanggal': '....................',
    'no_laporan':'',
    'sprin_date':'',
    'row_personel': []
    }
    engine = db.engine
    connection = engine.raw_connection()

    row_personel = []

    try:
        # print("ambil SPRIN")
        
        cursprin = connection.cursor()
        strSQL = "SELECT a.id, a.no_sprin, a.tanggal_sprin, b.no_laporan "
        strSQL += " FROM sprin_cakum_pelaporan b inner JOIN sprin_cakum a "
        strSQL += " ON b.id = a.sprin_pelaporan_id WHERE a.no_sprin= '" + no_sprin + "'"
        # strParam = (no_laporan)
        # cursprin.execute(strSQL, strParam)
        cursprin.execute(strSQL)
        recsprin = cursprin.fetchone()
       
        if(recsprin):
            print("RECORD SPRIN")
            id_sprin_cakum = recsprin[0]
            
            no_sprin = recsprin[1]
            no_laporan = recsprin[3]

            context['no_sprin'] = no_sprin
            context['tanggal_sprin'] = recsprin[2]
            context['no_laporan'] = no_laporan
            curdok = connection.cursor()
            strSQL = "SELECT dok_tanggal, dok_no, polres.polres FROM sprin_cakum_dokumen"
            strSQL += " LEFT JOIN polres ON sprin_cakum_dokumen.wilkum_id = polres.id"
            strSQL += " WHERE sprin_cakum_id = '" + str(id_sprin_cakum) + "' AND tipe_dokumen = 'DSR'"
            curdok.execute(strSQL)
            recdok = curdok.fetchone()
            # print("SELESAI RECDOK")
            if (recdok):
                # print("RECORD DOK")
                context['permohonan_tanggal'] = recdok[0]
                context['permohonan_no'] = recdok[1]
                context['permohonan_wilayah'] = recdok[2]
                curdok.close()
            curpersonel = connection.cursor()
            strSQL = "select a.no_sprin,c.nrp,c.full_name,d.nama_satwa, b.jabatan_id, f.pangkat, g.asset_kategori "
            strSQL += " FROM sprin_cakum a inner join sprin_cakum_personel b  on a.id = b.sprin_cakum_id"
            strSQL += " inner join user_profiles c on b.personel_id = c.id"
            strSQL += " left outer join asset_satwa d on b.satwa_id = d.id"
            # strSQL += " left outer join jabatan e on b.jabatan_id = e.id"
            strSQL += " left outer join pangkat f on b.pangkat_id = f.id"
            strSQL += " left outer join asset_satwa_kategori g on d.asset_kategori_id = g.id"
            strSQL += " WHERE a.id = '" + str(id_sprin_cakum) + "'"
            curpersonel.execute(strSQL)
            rowpersonel = curpersonel.fetchall()
            iloop = 0
            for item in rowpersonel:
                iloop += 1
                item_personel = {
                    'no': '',
                    'full_name'
                    'pangkat': '',
                    'nrp': '',  
                    'nama_satwa': '',
                    'jabatan':''  
                }
                item_personel['no'] = iloop
                item_personel['nrp'] = item[1]
                item_personel['full_name'] = item[2]
                item_personel['nama_satwa'] = item[3]
                item_personel['jabatan'] = item[4]
                item_personel['pangkat'] = item[5]
                item_personel['asset_kategori'] = item[6]
                row_personel.append(item_personel)
            context['row_personel'] = row_personel
            
            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.constants.ERROR_CORRECT_L,
                box_size=10,
                border=4,
                )
            qr.add_data(qrpath + str(no_sprin))
            qr.make(fit=True)

            img_size = Cm(3)  # sets the size of the image
            img = qr.make_image(fill_color="black", back_color="white")
            img.save(result_path + "/qrcode" + no_sprin.replace('/','_').replace('\\','_') + ".bmp")

            signature = result_path +"/qrcode" + no_sprin.replace('/','_').replace('\\','_') + ".bmp"
            sign = InlineImage(doc, signature,img_size)
            context['qrcode'] = sign  # adds the InlineImage object to the context
    
            doc.render(context)

            filename = "SPRIN_" + no_sprin.replace('/','_').replace('\\','_') + ".docx"
            filepath = result_path + "/" + filename
            doc.save(filepath)
            encoded_content = helper.file_to_base64(filepath)
            doc_json["file_name"] = filename
            doc_json["mime"] = "application/octet-stream"
            doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")
        cursprin.close()
    except Exception as error:
        logger.exception(error)
    finally:
        if(connection):
            connection.close()
            
    return doc_json


#@app.get("/doc_cakum_peristiwa/{no_laporan}")
def createdoc_peristiwa(no_laporan:str):
    doc = DocxTemplate(path + "/CAKUM_PERISTIWA.docx")
    context = {
    'nama_pelapor': '',
    'pekerjaan_pelapor': '..........................',
    'alamat_pelapor': '........................',
    'peristiwa': '....................',
    'tempat_kejadian':'..........................',
    'waktu_kejadian':'..........................',
    'korban_nama':'..........................',
    'korban_umur':'..........................',
    'korban_jenis_kelamin':'..........................',
    'korban_suku':'..........................',
    'korban_pekerjaan':'..........................',
    'korban_alamat':'..........................',
    'korban_materiil':'..........................',
    'wilayah_kepolisian':'..........................',
    'keadaan_tkp':'..........................',
    'nopol_kendaraan':'..........................',
    'pajaga_penerima':'..........................',
    'created_date':'..........................',
    }
    engine = db.engine
    connection = engine.raw_connection()

    blankdot = '..............................................................'
    doc_json = {}
    try:
        # print("ambil SPRIN")
        
        cursprin = connection.cursor()
        strSQL = "SELECT "
        strSQL += " nama_pelapor c0,pekerjaan_pelapor c1,alamat_pelapor c2,"
        strSQL += " peristiwa c3,tempat_kejadian c4,waktu_kejadian c5,"
        strSQL += " korban_nama c6,korban_umur c7,korban_jenis_kelamin c8,"
        strSQL += " korban_suku c9,korban_pekerjaan c10,korban_alamat c11,"
        strSQL += " korban_materiil c12,wilayah_kepolisian c13,keadaan_tkp c14,"
        strSQL += " nopol_kendaraan c15,pajaga_penerima c16,created_date c17"
        strSQL += " FROM sprin_cakum_pelaporan"
        strSQL += " WHERE no_laporan = '" + no_laporan + "'"
        # strParam = (no_laporan)
        # cursprin.execute(strSQL, strParam)
        cursprin.execute(strSQL)
        recsprin = cursprin.fetchone()

        if(recsprin):
            # print("RECORD SPRIN")
            context['no_laporan'] = no_laporan
            context['nama_pelapor'] = recsprin[0] if (recsprin[0]) else blankdot
            context['pekerjaan_pelapor'] = recsprin[1] if (recsprin[1]) else blankdot
            context['alamat_pelapor'] = recsprin[2] if (recsprin[2]) else blankdot
            context['peristiwa'] = recsprin[3] if (recsprin[3]) else blankdot
            context['tempat_kejadian'] = recsprin[4] if (recsprin[4]) else blankdot
            context['waktu_kejadian'] = recsprin[5].strftime('%d-%m-%Y %H:%M') if (recsprin[5]) else blankdot
            context['korban_nama'] = recsprin[6] if (recsprin[6]) else blankdot
            context['korban_umur'] = recsprin[7] if (recsprin[7]) else blankdot
            context['korban_jenis_kelamin'] = recsprin[8] if (recsprin[8]) else blankdot
            context['korban_suku'] = recsprin[9] if (recsprin[9]) else blankdot
            context['korban_pekerjaan'] = recsprin[10] if (recsprin[10]) else blankdot
            context['korban_alamat'] = recsprin[11] if (recsprin[11]) else blankdot
            context['korban_materiil'] = recsprin[12] if (recsprin[12]) else blankdot
            context['wilayah_kepolisian'] = recsprin[13] if (recsprin[13]) else blankdot
            context['keadaan_tkp'] = recsprin[14] if (recsprin[14]) else blankdot
            context['nopol_kendaraan'] = recsprin[15] if (recsprin[15]) else blankdot
            context['pajaga_penerima'] = recsprin[16] if (recsprin[16]) else blankdot
            context['created_date'] = recsprin[17].strftime('%d-%m-%Y') if (recsprin[17]) else '............'
            doc.render(context)


            filename = "PERISTIWA_" + no_laporan.replace('/','_').replace('\\','_') + ".docx"
            filepath = result_path + "/" + filename
            doc.save(filepath)
            encoded_content = helper.file_to_base64(filepath)
            doc_json["file_name"] = filename
            doc_json["mime"] = "application/octet-stream"
            doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")
        cursprin.close()
    except Exception  as error:
        logger.exception(error)
    finally:
        if(connection):
            connection.close()
    return doc_json

#@app.get("/doc_cakum_laporan_singkat/{no_sprin}")
def createdoc_laporan_singkat(no_sprin:str):

    doc = DocxTemplate(path + "/CAKUM_LAPORAN_SINGKAT.docx")

    context = {
    'no_laporan': '..........................',
    'tanggal_laporan': '..........................',
    'tempat_kejadian': '..........................',
    'uraian': [],
    'kesimpulan': [],
    'hambatan': [],
    'katim_nama': '..........................',
    'katim_pangkat': '..........................',
    'katim_nrp': '..........................',
    'nama_polisi_pendamping': '..........................',
    'nrp_polisi_pendamping': '..........................',
    'row_personel': [],
    'row_satwa': [],
    'created_date':'..........................',
    }
    engine = db.engine
    connection = engine.raw_connection()
    doc_json = {}
    respdata = []
    
    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']

    try:
        curcakum = connection.cursor()
        strSQL = "SELECT sprin.id,"
        strSQL += " a.no_laporan, b.tanggal_laporan, a.tempat_kejadian, "
        strSQL += " b.uraian_singkat, b.kesimpulan, b.hambatan, b.nama_polisi_pendamping, b.nrp_polisi_pendamping, "
        strSQL += " c.full_name katim_nama, c.nrp katim_nrp,"
        strSQL += " d.pangkat"
        strSQL += " FROM sprin_cakum sprin"
        strSQL += " INNER JOIN sprin_cakum_pelaporan a ON sprin.sprin_pelaporan_id = a.id"
        strSQL += " LEFT OUTER JOIN sprin_cakum_laporan b ON sprin.id = b.sprin_cakum_id"
        strSQL += " LEFT OUTER JOIN user_profiles c ON c.id = a.katim_id"
        strSQL += " LEFT OUTER JOIN pangkat d ON d.id = c.pangkat_id"
        strSQL += " WHERE sprin.no_sprin = '" + no_sprin + "'"
        curcakum.execute(strSQL)
        reccakum = curcakum.fetchone()
        if(curcakum):
            id_sprin_cakum = reccakum[0]
            context['no_laporan'] = reccakum[1]
            context['tempat_kejadian'] = reccakum[3]
            
            tanggal_laporan = reccakum[2]
            if(tanggal_laporan):
                hari = nama_hari[tanggal_laporan.weekday()]
                context['tanggal'] = tanggal_laporan.day
                context['bulan'] = tanggal_laporan.month
                context['tahun'] = tanggal_laporan.year
                context['pukul'] = str(tanggal_laporan.time())[0:5]
                context['tahun'] = tanggal_laporan.year
                context['tanggal_laporan'] = hari + ',' + str(tanggal_laporan.day) + '-' + nama_bulan[tanggal_laporan.month-1] + '-' + str(tanggal_laporan.year)


            split_uraian = reccakum[4].split('\n')
            row_uraian = []
            for itext in split_uraian:
                row_uraian.append(itext)
            context['uraian'] = row_uraian

            split_kesimpulan = reccakum[5].split('\n')
            row_kesimpulan = []
            for itext in split_kesimpulan:
                row_kesimpulan.append(itext)
            context['kesimpulan'] = row_kesimpulan

            split_hambatan = reccakum[6].split('\n')
            row_hambatan = []
            for itext in split_hambatan:
                row_hambatan.append(itext)
            context['hambatan'] = row_hambatan

            context['nama_polisi_pendamping'] = reccakum[7]
            context['nrp_polisi_pendamping'] = reccakum[8]
            context['katim_nama'] = reccakum[9]
            context['katim_nrp'] = reccakum[10]
            context['katim_pangkat'] = reccakum[11]
        else:
            respdata.append("ERROR")
            respdata.append("Tidak ada NO SPRIN")
            curcakum.close()
            connection.close()
            return respdata
        curpersonel = connection.cursor()
        strSQL = "select a.no_sprin,c.nrp,c.full_name,d.nama_satwa, b.jabatan_id, f.pangkat, g.asset_kategori "
        strSQL += " FROM sprin_cakum a inner join sprin_cakum_personel b  on a.id = b.sprin_cakum_id"
        strSQL += " inner join user_profiles c on b.personel_id = c.id"
        strSQL += " left outer join asset_satwa d on b.satwa_id = d.id"
        # strSQL += " left outer join jabatan e on b.jabatan_id = e.id"
        strSQL += " left outer join pangkat f on b.pangkat_id = f.id"
        strSQL += " left outer join asset_satwa_kategori g on d.asset_kategori_id = g.id"
        strSQL += " WHERE a.id = '" + str(id_sprin_cakum) + "'"
        # return(strSQL)
        curpersonel.execute(strSQL)
        rowpersonel = curpersonel.fetchall()
        iloop = 0
        row_personel = []
        row_satwa = []
        for item in rowpersonel:
            iloop += 1
            item_personel = {
                'no':'..........................',
                'full_name':'..........................',
                'pangkat':'..........................',
                'nrp':'..........................',
                 'jabatan':'..........................',
                }
            item_satwa = {
                'nama_satwa':'..........................',
                'asset_kategori':'..........................',
            }
            item_personel['no'] = iloop
            item_personel['nrp'] = item[1]
            item_personel['full_name'] = item[2]
            # item_personel['nama_satwa'] = item[3]
            item_personel['jabatan'] = item[4]
            item_personel['pangkat'] = item[5]
            # item_personel['asset_kategori'] = item[6]
            # print(item_personel)
            row_personel.append(item_personel)
            if (item[3]):
                item_satwa['nama_satwa'] = item[3]
                item_satwa['asset_kategori'] = item[6]
                row_satwa.append(item_satwa)
        # print(row_personel)
        context['row_personel'] = row_personel
        context['row_satwa'] = row_satwa
        curpersonel.close()

        curcakum.close()
        connection.close()

        doc.render(context)


        filename = "LAPORAN_SINGKAT_" + no_sprin.replace('/','_').replace('\\','_') + ".docx"
        filepath = result_path + "/" + filename
        doc.save(filepath)
        encoded_content = helper.file_to_base64(filepath)
        doc_json["file_name"] = filename
        doc_json["mime"] = "application/octet-stream"
        doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")
    except Exception  as error:
        logger.exception(error)
    finally:
        if(connection):
            connection.close()
    return doc_json

#@app.get("/doc_cakum_laporan/{no_sprin}")
def createdoc_laporan(no_sprin:str):

    doc = DocxTemplate(path + "/CAKUM_LAPORAN.docx")

    context = {
    'no_laporan': '..........................',
    'hari': '..........................',
    'tanggal': '..........................',
    'bulan': '..........................',
    'tahun': '..........................',
    'pukul': '..........................',
    'no_sprin':'..........................',
    'tanggal_sprin':'..........................',
    'tanggal_paraf': '..........................',
    'katim_nama': '..........................',
    'katim_pangkat': '..........................',
    'katim_nrp': '..........................',
    'nama_polisi_pendamping': '..........................',
    'nrp_polisi_pendamping': '..........................',
    'row_personel': [],
    'isi_laporan':[],
    'jumlah_anggota':'..........................',
    'jumlah_satwa':'..........................',
    'nama_satwa': '..........................',
    'created_date':'..........................',
    'sketsa':'',
    }
    engine = db.engine
    connection = engine.raw_connection()

    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    doc_json = {}
    try:
        curcakum = connection.cursor()
        strSQL = "SELECT sprin.id,"
        strSQL += " a.no_laporan, b.tanggal_laporan, a.tempat_kejadian, "
        strSQL += " b.uraian_singkat, b.kesimpulan, b.hambatan, b.nama_polisi_pendamping, b.nrp_polisi_pendamping, b.jabatan_polisi_pendamping, b.kesatuan_polisi_pendamping,"
        strSQL += " c.full_name katim_nama, c.nrp katim_nrp,"
        strSQL += " d.pangkat"
        strSQL += " ,b.isi_laporan"
        strSQL += " , sprin.no_sprin, sprin.tanggal_sprin"
        strSQL += " FROM sprin_cakum sprin"
        strSQL += " INNER JOIN sprin_cakum_pelaporan a ON sprin.sprin_pelaporan_id = a.id"
        strSQL += " LEFT OUTER JOIN sprin_cakum_laporan b ON sprin.id = b.sprin_cakum_id"
        strSQL += " LEFT OUTER JOIN user_profiles c ON c.id = a.katim_id"
        strSQL += " LEFT OUTER JOIN pangkat d ON d.id = c.pangkat_id"
        strSQL += " WHERE sprin.no_sprin = '" + no_sprin + "'"
        curcakum.execute(strSQL)
        reccakum = curcakum.fetchone()
        if(curcakum):
            id_sprin_cakum = reccakum[0]
            context['no_laporan'] = reccakum[1]
            tanggal_laporan = reccakum[2]
            if(tanggal_laporan):
                context['hari'] = nama_hari[tanggal_laporan.weekday()]
                context['tanggal'] = tanggal_laporan.day
                context['bulan'] = tanggal_laporan.month
                context['tahun'] = tanggal_laporan.year
                context['pukul'] = str(tanggal_laporan.time())[0:5]
                context['tahun'] = tanggal_laporan.year
                context['tanggal_paraf'] = str(tanggal_laporan.day) + '-' + nama_bulan[tanggal_laporan.month-1] + '-' + str(tanggal_laporan.year)

            context['no_sprin'] = reccakum[15]
            tanggal_sprin = reccakum[16]
            if (tanggal_sprin):
                context['tanggal_sprin'] = str(tanggal_sprin.day) + '-' + nama_bulan[tanggal_sprin.month-1] + '-' + str(tanggal_sprin.year)
            
            context['tanggal_laporan'] = reccakum[2]
            context['tempat_kejadian'] = reccakum[3]

            if (reccakum[4]):
                split_uraian = reccakum[4].split('\n')
                row_uraian = []
                for itext in split_uraian:
                    row_uraian.append(itext)
                context['uraian'] = row_uraian

            if (reccakum[5]):
                split_kesimpulan = reccakum[5].split('\n')
                row_kesimpulan = []
                for itext in split_kesimpulan:
                    row_kesimpulan.append(itext)
                context['kesimpulan'] = row_kesimpulan

            if (reccakum[6]):
                split_hambatan = reccakum[6].split('\n')
                row_hambatan = []
                for itext in split_hambatan:
                    row_hambatan.append(itext)
                context['hambatan'] = row_hambatan

            context['nama_polisi_pendamping'] = reccakum[7]
            context['nrp_polisi_pendamping'] = reccakum[8]
            context['jabatan_polisi_pendamping'] = reccakum[9]
            context['kesatuan_polisi_pendamping'] = reccakum[10]
            context['katim_nama'] = reccakum[11]
            context['katim_nrp'] = reccakum[12]
            context['katim_pangkat'] = reccakum[13]

            if (reccakum[14]):
                split_laporantext = reccakum[14].split('\n')
                row_laporantext = []
                for itext in split_laporantext:
                    row_laporantext.append(itext)
                context['isi_laporan'] = row_laporantext

        else:
            curcakum.close()
            connection.close()

        curpersonel = connection.cursor()
        strSQL = "select a.no_sprin,c.nrp,c.full_name,d.nama_satwa, b.jabatan_id, f.pangkat, g.asset_kategori "
        strSQL += " FROM sprin_cakum a inner join sprin_cakum_personel b  on a.id = b.sprin_cakum_id"
        strSQL += " inner join user_profiles c on b.personel_id = c.id"
        strSQL += " left outer join asset_satwa d on b.satwa_id = d.id"
        # strSQL += " left outer join jabatan e on b.jabatan_id = e.id"
        strSQL += " left outer join pangkat f on b.pangkat_id = f.id"
        strSQL += " left outer join asset_satwa_kategori g on d.asset_kategori_id = g.id"
        strSQL += " WHERE a.id = '" + str(id_sprin_cakum) + "'"

        curpersonel.execute(strSQL)
        rowpersonel = curpersonel.fetchall()
        iloop = 0
        row_personel = []
        row_satwa = []
        comma_satwa = ""
        nama_satwa = ""

        for item in rowpersonel:
            iloop += 1
            item_personel = {
                'no':'..........................',
                'full_name':'..........................',
                'pangkat':'..........................',
                'nrp':'..........................',
                 'jabatan':'..........................',
                }
            item_satwa = {
                'nama_satwa':'..........................',
                'asset_kategori':'..........................',
            }
            item_personel['no'] = iloop
            item_personel['nrp'] = item[1]
            item_personel['full_name'] = item[2]
            # item_personel['nama_satwa'] = item[3]
            item_personel['jabatan'] = item[4]
            item_personel['pangkat'] = item[5]
            # item_personel['asset_kategori'] = item[6]
            # print(item_personel)
            row_personel.append(item_personel)
            if (item[3]):
                item_satwa['nama_satwa'] = item[3]
                item_satwa['asset_kategori'] = item[6]
                row_satwa.append(item_satwa)
                nama_satwa += comma_satwa + item[3]
                comma_satwa = ","
        jumlah_anggota = len(row_personel)
        jumlah_satwa = len(row_satwa)
        # print(row_personel)
        context['row_personel'] = row_personel
        context['row_satwa'] = row_satwa
        context['jumlah_anggota'] = jumlah_anggota
        context['jumlah_satwa'] = jumlah_satwa
        context['nama_satwa'] = nama_satwa
        curpersonel.close()
        
        cursketsa = connection.cursor()
        strSQL = "SELECT file, file_name, file_type, tipe_dokumen "
        strSQL += " FROM sprin_cakum_dokumen"
        strSQL += " WHERE sprin_cakum_id = '" + str(id_sprin_cakum) + "'"
        strSQL += " AND tipe_dokumen in ('SKT','DOK')"
        strSQL += " AND file_type LIKE 'image%';"
        cursketsa.execute(strSQL)
        row_sketsa = cursketsa.fetchall()
        arr_sketsa = []
        arr_dok = []
        for recsketsa in row_sketsa:
            filesketsa = result_path + "/SKETSA_" + no_sprin.replace('/','_').replace('\\','_') + "_" + recsketsa[1].replace(" ","")
            split_isifile = recsketsa[0].split(',')
            isifile = split_isifile[1]
            file_type = recsketsa[2]
            tipe_dokumen = recsketsa[3]
            img_data = bytes(isifile,'ascii')
            with open(filesketsa, "wb") as fh:
                fh.write(base64.decodebytes(img_data))
            width, height = get_image_size(filesketsa)
            img_size = Cm(19)  # sets the size of the image
            if (width > height):
                sign = InlineImage(doc, filesketsa,width=img_size)
            else:
                sign = InlineImage(doc, filesketsa,height=img_size)
            if (file_type[0:5]=='image'):
                if (tipe_dokumen=='SKT'):
                    arr_sketsa.append(sign)
                else:
                    arr_dok.append(sign)
        context['sketsa'] = arr_sketsa
        if (len(arr_dok) > 0):
            context['dokumen_judul'] = 'Dokumentasi'
            context['dokumen'] = arr_dok
            context['page_break'] = ['\n','\n','\n','\n','\n','\n','\n','\n','\n']
        cursketsa.close()
        
        curcakum.close()
        connection.close()
    except Exception as error:
        logger.error(error)
    finally:
        if(connection):
            connection.close()

    doc.render(context)
    filename = "LAPORAN_" + no_sprin.replace('/','_').replace('\\','_') + ".docx"
    filepath = result_path + "/" + filename
    doc.save(filepath)
    encoded_content = helper.file_to_base64(filepath)
    doc_json["file_name"] = filename
    doc_json["mime"] = "application/octet-stream"
    doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")
    return doc_json

def createdoc_cakum_wakbu(no_sprin:str):
    
    context = { }

    engine = db.engine
    connection = engine.raw_connection()

    respdata = []
    message = ''
    blankdot = '..............................................................'
    id_sprin_cakum = ''
    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    nama_romawi = ['I','II','III','IV','V']

    row_personel = []
    doc_json = {}
    try:
        curhead = connection.cursor()
        strSQL = "SELECT a.id, a.tanggal_sprin, b.wilayah_kepolisian, c.tanggal_laporan"
        strSQL += " FROM sprin_cakum a LEFT JOIN sprin_cakum_pelaporan b ON a.id = b.sprin_cakum_id"
        strSQL += " LEFT JOIN sprin_cakum_laporan c ON a.id = c.sprin_cakum_id"
        strSQL += " WHERE a.no_sprin = '" + str(no_sprin) + "'"
        curhead.execute(strSQL)
        rechead = curhead.fetchone()
        if rechead:
            id_sprin_cakum = rechead[0]

            tanggal_sprin = ""
            if rechead[1]:
                tanggal = rechead[1].day
                bulan = rechead[1].month
                tahun = rechead[1].year
                tanggal_sprin = str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)
            
            polres = rechead[2]

            tanggal_laporan = ""
            if rechead[3]:
                tanggal = rechead[3].day
                bulan = rechead[3].month
                tahun = rechead[3].year
                tanggal_laporan = str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)
            
            context['tanggal_laporan'] = tanggal_laporan
            context['polres'] = polres if polres else "-"

            curpersonel = connection.cursor()
            strSQL = "select a.no_sprin,c.nrp,c.full_name,d.nama_satwa, b.jabatan_id, f.pangkat, g.asset_kategori "
            strSQL += " FROM sprin_cakum a inner join sprin_cakum_personel b  on a.id = b.sprin_cakum_id"
            strSQL += " inner join user_profiles c on b.personel_id = c.id"
            strSQL += " left outer join asset_satwa d on b.satwa_id = d.id"
            strSQL += " left outer join pangkat f on b.pangkat_id = f.id"
            strSQL += " left outer join asset_satwa_kategori g on d.asset_kategori_id = g.id"
            strSQL += " WHERE a.id = '" + str(id_sprin_cakum) + "'"
            curpersonel.execute(strSQL)
            rowpersonel = curpersonel.fetchall()
            iloop = 0
            for item in rowpersonel:
                iloop += 1
                item_personel = {
                    'no': '',
                    'full_name'
                    'pangkat': '',
                    'nrp': '',  
                    'nama_satwa': '',
                    'jabatan':''  
                }
                item_personel['no'] = iloop
                item_personel['nrp'] = item[1]
                item_personel['full_name'] = item[2]
                item_personel['nama_satwa'] = item[3]
                item_personel['jabatan'] = item[4]
                item_personel['pangkat'] = item[5]
                item_personel['asset_kategori'] = item[6]
                row_personel.append(item_personel)
            context['personel'] = row_personel
            tot_dana = 5000 * len(row_personel)
            tot_terima = 47000 * len(row_personel)
            context['tot_dana'] = "{:,}".format(tot_dana).replace(',','.')
            context['tot_terima'] = "{:,}".format(tot_terima).replace(',','.')

    except Exception  as error:
        logger.exception(error)
    finally:
        if(connection):
            connection.close()

    doc = DocxTemplate(path + "/CAKUM_WABKU.docx")

    doc.render(context)
    filename = "WABKU_" + no_sprin.replace('/','_').replace('\\','_') + ".docx"
    filepath = result_path + "/" + filename
    doc.save(filepath)
    encoded_content = helper.file_to_base64(filepath)
    doc_json["file_name"] = filename
    doc_json["mime"] = "application/octet-stream"
    doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")
    return doc_json


def createdoc_cakum_absen(no_sprin: str):
    context = {}

    engine = db.engine
    connection = engine.raw_connection()

    respdata = []
    message = ''
    blankdot = '..............................................................'
    id_sprin_cakum = ''
    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober',
                  'November', 'Desember']
    nama_romawi = ['I', 'II', 'III', 'IV', 'V']

    row_personel = []
    doc_json = {}
    try:
        curhead = connection.cursor()
        strSQL = "SELECT a.id, a.tanggal_sprin, b.wilayah_kepolisian, c.tanggal_laporan"
        strSQL += " FROM sprin_cakum a LEFT JOIN sprin_cakum_pelaporan b ON a.id = b.sprin_cakum_id"
        strSQL += " LEFT JOIN sprin_cakum_laporan c ON a.id = c.sprin_cakum_id"
        strSQL += " WHERE a.no_sprin = '" + str(no_sprin) + "'"
        curhead.execute(strSQL)
        rechead = curhead.fetchone()
        if rechead:
            id_sprin_cakum = rechead[0]

            tanggal_sprin = ""
            if rechead[1]:
                tanggal = rechead[1].day
                bulan = rechead[1].month
                tahun = rechead[1].year
                tanggal_sprin = str(tanggal) + ' ' + nama_bulan[bulan - 1] + ' ' + str(tahun)

            polres = rechead[2]

            tanggal_laporan = ""
            if rechead[3]:
                tanggal = rechead[3].day
                bulan = rechead[3].month
                tahun = rechead[3].year
                tanggal_laporan = str(tanggal) + ' ' + nama_bulan[bulan - 1] + ' ' + str(tahun)

            context['tanggal_laporan'] = tanggal_laporan
            context['polres'] = polres if polres else "-"

            curpersonel = connection.cursor()
            strSQL = "select a.no_sprin, c.full_name, f.pangkat, c.nrp, d.nama_satwa, b.jabatan_id, g.asset_kategori "
            strSQL += " FROM sprin_cakum a inner join sprin_cakum_personel b  on a.id = b.sprin_cakum_id"
            strSQL += " inner join user_profiles c on b.personel_id = c.id"
            strSQL += " left outer join asset_satwa d on b.satwa_id = d.id"
            strSQL += " left outer join pangkat f on b.pangkat_id = f.id"
            strSQL += " left outer join asset_satwa_kategori g on d.asset_kategori_id = g.id"
            strSQL += " WHERE a.id = '" + str(id_sprin_cakum) + "'"
            curpersonel.execute(strSQL)
            rowpersonel = curpersonel.fetchall()
            iloop = 0
            for item in rowpersonel:
                iloop += 1
                item_personel = {
                    'no': '',
                    'full_name'
                    'pangkat': '',
                    'nrp': ''
                }
                item_personel['no'] = iloop
                item_personel['full_name'] = item[1]
                item_personel['pangkat'] = item[2]
                item_personel['nrp'] = item[3]
                row_personel.append(item_personel)
            context['personel'] = row_personel
    except Exception  as error:
        logger.exception(error)
    finally:
        if (connection):
            connection.close()

    doc = DocxTemplate(path + "/CAKUM_ABSEN.docx")
    doc.render(context)
    filename = "ABSEN_" + no_sprin.replace('/','_').replace('\\','_') + ".docx"
    filepath = result_path + "/" + filename
    doc.save(filepath)
    encoded_content = helper.file_to_base64(filepath)
    doc_json["file_name"] = filename
    doc_json["mime"] = "application/octet-stream"
    doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")
    return doc_json