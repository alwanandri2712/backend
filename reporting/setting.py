import os
import logging
import os

logger =  logging.getLogger('werkzeug')


dir_path = os.path.dirname(os.path.realpath(__file__)) + "/../.."
root_dir = os.path.abspath(dir_path)
app_dir = root_dir + "/app"