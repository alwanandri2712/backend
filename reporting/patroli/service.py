from datetime import datetime
from docxtpl import DocxTemplate, InlineImage
import app.helpers.general as helper
from app import db
from app.reporting.setting import app_dir,logger
from docx.shared import Cm
import qrcode

path = app_dir + "/reporting/patroli"
result_path = path + '/result'
qrpath = 'http://207.148.116.191/#report/SPT/'


def createdoc_sprin_patroli(no_sprin:str):

    context = {
    'no_sprin': '..........................',
    'tanggal_awal_akhir':'..........................',
    'jam_tugas':'..........................',
    'row_wilkum':[],
    }

    engine = db.engine
    connection = engine.raw_connection();

    respdata = []
    message = ''
    blankdot = '..............................................................'
    id_sprin_cakum = ''
    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    nama_romawi = ['I','II','III','IV','V']

    row_wilkum = []

    tanggal_now = datetime.now()
    tanggal = str(tanggal_now.day)
    bulan = nama_bulan[tanggal_now.month-1]
    tahun = str(tanggal_now.year)
    tanggal_cetak = tanggal + "-" + bulan + "-" + tahun
    context['tanggal_cetak'] = tanggal_cetak

    try:
        curtgl = connection.cursor()
        strSQL = "SELECT a.id,a.no_sprin, a.tanggal_sprin,a.tugas_awal,a.tugas_akhir,a.jam_tugas"
        strSQL += " ,b.tanggal_patroli, b.id, b.wilkum_area, c.polres"
        strSQL += " from sprin_patroli a inner join sprin_patroli_grup b on a.id = b.sprin_patroli_id"
        strSQL += " INNER JOIN polres c on b.wilkum_id = c.id"
        strSQL += " WHERE a.no_sprin = '" + no_sprin + "'"
        strSQL += " order by b.tanggal_patroli"
        curtgl.execute(strSQL)
        rowtgl = curtgl.fetchall()
        prevtgl = ""
        no_romawi = 0
        for rectgl in rowtgl:
            if (prevtgl != rectgl[2]):
                no_romawi = 0
            else:
                no_romawi += 1
            if (no_romawi>1):
                no_romawi = 0
            prevtgl = rectgl[2]
            no_wilkum = nama_romawi[no_romawi]
            
            sprin_patroli_id = rectgl[0]
            no_sprin = rectgl[1]
            
            if (rectgl[2]):
                tanggal = str(rectgl[2].day)
                bulan = nama_bulan[rectgl[2].month - 1]
                tahun = str(rectgl[2].year)
                tanggal_sprin = tanggal + "-" + bulan + "-" + tahun
                context['tanggal_sprin'] = tanggal_sprin

            context['no_sprin'] = no_sprin

            jam_tugas = rectgl[5]

            context['jam_tugas'] = jam_tugas

            tanggal = str(rectgl[6].day)
            bulan = nama_bulan[rectgl[6].month-1]
            tahun = str(rectgl[6].year)
            tanggal_patroli = tanggal + "-" + bulan + "-" + tahun

            grup_id = rectgl[7]
            wilkum_area = rectgl[8]
            polres = rectgl[9]
            
            line_wilkum = {
                'tanggal_patroli':tanggal_patroli,
                'jam_patroli':jam_tugas,
                'wilkum':polres,
                'wilayah':wilkum_area,
                'no':no_wilkum,
                'personel':[],
            }
            curpersonel = connection.cursor()
            strSQL = "SELECT b.jabatan, c.nama_satwa, d.pangkat, a.keterangan, e.full_name,e.nrp"
            strSQL += " FROM sprin_patroli_personel a LEFT OUTER JOIN jabatan b on a.jabatan_id = b.id"
            strSQL += " LEFT OUTER JOIN asset_satwa c ON a.satwa_id = c.id"
            strSQL += " LEFT OUTER JOIN pangkat d ON a.pangkat_id = d.id"
            strSQL += " LEFT OUTER JOIN user_profiles e ON a.profile_id = e.id"
            strSQL += " WHERE a.grup_id = '" + str(grup_id) + "' AND sprin_patroli_id = '" + str(sprin_patroli_id) + "'"
            curpersonel.execute(strSQL)
            row_personel = curpersonel.fetchall()
            arrpersonel = []
            no_personel = 1
            for recpersonel in row_personel:
                line_personel = {
                    'jabatan':recpersonel[0],
                    'satwa':recpersonel[1] if recpersonel[1] else "",
                    'pangkat':recpersonel[2],
                    'keterangan':recpersonel[3] if recpersonel[3] else "",
                    'nama':recpersonel[4],
                    'nrp':recpersonel[5],
                    'no': str(no_personel)
                }
                no_personel += 1
                arrpersonel.append(line_personel)
            line_wilkum['personel'] = arrpersonel
            curpersonel.close()

            row_wilkum.append(line_wilkum)

        curtgl.close()
                
    except Exception as error:
        logger.exception(error)
    finally:
        if(connection):
            connection.close()

    result = []

    line_wilkum = {}
    iloop = 0
    tgl_prev = datetime.now()
    awal = 0
    cek_group = 1
    cek_personel = 1
    group_loop = 0
    tugas_awal = ""
    tugas_akhir = ""
    for line_wilkum in row_wilkum:
        iloop += 1
        group_loop += 1
        tgl_now = line_wilkum['tanggal_patroli'] + ''
        if (group_loop == 1):
            tugas_awal = tgl_now
        tugas_akhir = tgl_now
        context['tanggal_patroli_' + str(group_loop)] = tgl_now
        context['jam_patroli_' + str(group_loop)] = str(line_wilkum['jam_patroli'])
        context['wilkum_' + str(group_loop)] = str(line_wilkum['wilkum'])
        context['wilayah_' + str(group_loop)] = str(line_wilkum['wilayah'])
        if (tgl_prev != tgl_now):
            if (awal!=0):
                if(iloop!=2):
                    result.append(tgl_prev)
                    result.append(tgl_now)
                    result.append(awal)
                    result.append(iloop)
                    cek_group = 0
                    break
            iloop = 0
        awal = 1
        tgl_prev = tgl_now
        line_personel = {}
        ipersonel = 0
        for line_personel in line_wilkum['personel']:
            ipersonel += 1
            context['c' + str(group_loop) + str(ipersonel) + '_1'] = str(line_personel['nama'])
            context['c' + str(group_loop) + str(ipersonel) + '_2'] = str(line_personel['pangkat'])
            context['c' + str(group_loop) + str(ipersonel) + '_3'] = str(line_personel['nrp'])
            context['c' + str(group_loop) + str(ipersonel) + '_4'] = str(line_personel['jabatan'])
            context['c' + str(group_loop) + str(ipersonel) + '_5'] = str(line_personel['satwa'])
            context['c' + str(group_loop) + str(ipersonel) + '_6'] = str(line_personel['keterangan'])
        if (ipersonel !=5):
            cek_personel = 0
   
    context['tanggal_awal_akhir'] = str(tugas_awal) + ' s.d. ' + str(tugas_akhir)

    cocok = 0
    if (cek_group==1) and (cek_personel==1) and (group_loop==12):
        cocok = 1
        doc = DocxTemplate(path + "/PATROLI_RAPI.docx")
    else:
        doc = DocxTemplate(path + "/PATROLI.docx")

    context['row_wilkum'] = row_wilkum

    qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.constants.ERROR_CORRECT_L,
                box_size=10,
                border=4,
                )
    qr.add_data(qrpath + str(no_sprin))
    qr.make(fit=True)

    img_size = Cm(3)  # sets the size of the image
    img = qr.make_image(fill_color="black", back_color="white")
    img.save(result_path + "/qrcode" + no_sprin.replace('/','_').replace('\\','_') + ".bmp")

    signature = result_path +"/qrcode" + no_sprin.replace('/','_').replace('\\','_') + ".bmp"
    sign = InlineImage(doc, signature,img_size)
    context['qrcode'] = sign  # adds the InlineImage object to the context
    
    doc.render(context)

    filename = "PATROLI_" + no_sprin.replace('/','_').replace('\\','_') + ".docx"
    filepath = result_path + "/" + filename
    doc.save(filepath)

    doc_json = dict()
    encoded_content = helper.file_to_base64(filepath)
    doc_json["file_name"] = filename
    doc_json["mime"] = "application/octet-stream"
    doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")

    return doc_json
    
def createdoc_patroli_laporan(grup_id:str):


    context = { }

    engine = db.engine
    connection = engine.raw_connection();

    respdata = []
    message = ''
    blankdot = '..............................................................'
    id_sprin_cakum = ''
    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    nama_romawi = ['I','II','III','IV','V']

    row_personel = []

    tanggal_now = datetime.now()
    tanggal = str(tanggal_now.day)
    bulan = nama_bulan[tanggal_now.month - 1]
    tahun = str(tanggal_now.year)
    tanggal_cetak = tanggal + "-" + bulan + "-" + tahun
    context['tanggal_cetak'] = tanggal_cetak

    try:
        curhead = connection.cursor()
        strSQL = "SELECT a.id, a.no_sprin, a.tanggal_sprin, a.tugas_awal, a.tugas_akhir,a.jam_tugas"
        strSQL += " ,b.wilkum_area, b.tanggal_patroli"
        strSQL += " ,c.route, c.tugas, c.saran, c.penutup"
        strSQL += " ,d.polres, c.created_date"
        strSQL += " FROM sprin_patroli a INNER JOIN sprin_patroli_grup b ON a.id = b.sprin_patroli_id"
        strSQL += " INNER JOIN sprin_patroli_laporan c ON b.id = c.grup_id"
        strSQL += " INNER JOIN polres d ON d.id = b.wilkum_id"
        strSQL += " WHERE b.id = '" + str(grup_id) + "'"
        curhead.execute(strSQL)
        rechead = curhead.fetchone()
        if rechead:
            sprin_patroli_id = rechead[0]
            no_sprin = rechead[1]
            tanggal_sprin_date = rechead[2]
            tugas_awal = rechead[3]
            tugas_akhir = rechead[4]
            jam_tugas = rechead[5]
            wilkum_area = rechead[6]
            tanggal_patroli = rechead[7]
            route = rechead[8]
            tugas = rechead[9]
            saran = rechead[10]
            penutup = rechead[11]
            polres = rechead[12]
            created_date = rechead[13]

            context['no_sprin'] = no_sprin

            hari = nama_hari[tanggal_sprin_date.weekday()]
            tanggal = tanggal_sprin_date.day
            bulan = tanggal_sprin_date.month
            tahun = tanggal_sprin_date.year

            context['tanggal_hari_sprin'] = hari + ', ' + str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)

            context['tanggal_sprin'] = str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)
            context['jam_tugas'] = jam_tugas
            context['polres'] = polres

            hari = nama_hari[created_date.weekday()]
            tanggal = created_date.day
            bulan = created_date.month
            tahun = created_date.year

            context['tanggal_laporan'] = hari + ', ' + str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)

            split_route = route.split('\n')
            row_route = []
            for itext in split_route:
                row_route.append(itext)
            context['route'] = row_route

            split_tugas = tugas.split('\n')
            row_tugas = []
            for itext in split_tugas:
                row_tugas.append(itext)
            context['tugas'] = row_tugas

            split_saran = saran.split('\n')
            row_saran = []
            for itext in split_saran:
                row_saran.append(itext)
            context['saran'] = row_saran

            split_penutup = penutup.split('\n')
            row_penutup = []
            for itext in split_penutup:
                row_penutup.append(itext)
            context['penutup'] = row_penutup

            curpersonel = connection.cursor()
            strSQL = "SELECT b.jabatan, c.nama_satwa, d.pangkat, a.keterangan, e.full_name,e.nrp"
            strSQL += " FROM sprin_patroli_personel a LEFT OUTER JOIN jabatan b on a.jabatan_id = b.id"
            strSQL += " LEFT OUTER JOIN asset_satwa c ON a.satwa_id = c.id"
            strSQL += " LEFT OUTER JOIN pangkat d ON a.pangkat_id = d.id"
            strSQL += " LEFT OUTER JOIN user_profiles e ON a.profile_id = e.id"
            strSQL += " WHERE a.grup_id = '" + str(grup_id) + "' AND sprin_patroli_id = '" + str(sprin_patroli_id) + "'"
            curpersonel.execute(strSQL)
            row_personel = curpersonel.fetchall()
            arrpersonel = []
            no_personel = 1
            for recpersonel in row_personel:
                line_personel = {
                    'jabatan':recpersonel[0] if recpersonel[0] else "",
                    'satwa':recpersonel[1] if recpersonel[1] else "",
                    'pangkat':recpersonel[2] if recpersonel[2] else "",
                    'keterangan':recpersonel[3] if recpersonel[3] else "",
                    'nama':recpersonel[4] if recpersonel[4] else "",
                    'nrp':recpersonel[5] if recpersonel[5] else "",
                    'no': str(no_personel) + '.'
                }
                if(recpersonel[0]):
                    if(recpersonel[0][:5]=='KATIM'):
                        context['katim'] = recpersonel[4] if recpersonel[4] else ""
                no_personel += 1
                arrpersonel.append(line_personel)
            context['personel'] = arrpersonel
            curpersonel.close()

    except Exception  as error:
        logger.error(error);
    finally:
        if(connection):
            connection.close()

    doc = DocxTemplate(path +"/PATROLI_LAPORAN.docx")

    doc.render(context)

    filename = "PATROLI_LAPORAN_" + grup_id.replace('/','_').replace('\\','_') + ".docx"
    filepath = result_path + "/" + filename
    doc.save(filepath)

    doc_json = dict()
    encoded_content = helper.file_to_base64(filepath)
    doc_json["file_name"] = filename
    doc_json["mime"] = "application/octet-stream"
    doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")

    return doc_json
    
def createdoc_patroli_wabku(grup_id:str):


    context = { }

    engine = db.engine
    connection = engine.raw_connection();

    respdata = []
    message = ''
    blankdot = '..............................................................'
    id_sprin_cakum = ''
    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    nama_romawi = ['I','II','III','IV','V']

    row_personel = []

    tanggal_now = datetime.now()
    tanggal = str(tanggal_now.day)
    bulan = nama_bulan[tanggal_now.month - 1]
    tahun = str(tanggal_now.year)
    tanggal_cetak = tanggal + "-" + bulan + "-" + tahun
    context['tanggal_cetak'] = tanggal_cetak

    try:
        curhead = connection.cursor()
        strSQL = "SELECT a.id, a.no_sprin, a.tanggal_sprin, a.tugas_awal, a.tugas_akhir,a.jam_tugas"
        strSQL += " ,b.wilkum_area, b.tanggal_patroli"
        strSQL += " ,c.route, c.tugas, c.saran, c.penutup"
        strSQL += " ,d.polres, c.created_date"
        strSQL += " FROM sprin_patroli a INNER JOIN sprin_patroli_grup b ON a.id = b.sprin_patroli_id"
        strSQL += " INNER JOIN sprin_patroli_laporan c ON b.id = c.grup_id"
        strSQL += " INNER JOIN polres d ON d.id = b.wilkum_id"
        strSQL += " WHERE b.id = '" + str(grup_id) + "'"
        curhead.execute(strSQL)
        rechead = curhead.fetchone()
        if rechead:
            sprin_patroli_id = rechead[0]
            no_sprin = rechead[1]
            tanggal_sprin_date = rechead[2]
            tugas_awal = rechead[3]
            tugas_akhir = rechead[4]
            jam_tugas = rechead[5]
            wilkum_area = rechead[6]
            tanggal_patroli = rechead[7]
            route = rechead[8]
            tugas = rechead[9]
            saran = rechead[10]
            penutup = rechead[11]
            polres = rechead[12]
            created_date = rechead[13]

            context['no_sprin'] = no_sprin

            hari = nama_hari[tanggal_patroli.weekday()]
            tanggal = tanggal_patroli.day
            bulan = tanggal_patroli.month
            tahun = tanggal_patroli.year

            context['tanggal_patroli'] = str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)

            context['tanggal_sprin'] = str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)
            context['jam_tugas'] = jam_tugas
            context['polres'] = polres

            hari = nama_hari[created_date.weekday()]
            tanggal = created_date.day
            bulan = created_date.month
            tahun = created_date.year

            context['created_date'] = hari + ', ' + str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)

            curpersonel = connection.cursor()
            strSQL = "SELECT b.jabatan, c.nama_satwa, d.pangkat, a.keterangan, e.full_name,e.nrp"
            strSQL += " FROM sprin_patroli_personel a LEFT OUTER JOIN jabatan b on a.jabatan_id = b.id"
            strSQL += " LEFT OUTER JOIN asset_satwa c ON a.satwa_id = c.id"
            strSQL += " LEFT OUTER JOIN pangkat d ON a.pangkat_id = d.id"
            strSQL += " LEFT OUTER JOIN user_profiles e ON a.profile_id = e.id"
            strSQL += " WHERE a.grup_id = '" + str(grup_id) + "' AND sprin_patroli_id = '" + str(sprin_patroli_id) + "'"
            curpersonel.execute(strSQL)
            row_personel = curpersonel.fetchall()
            arrpersonel = []
            no_personel = 1
            
            for recpersonel in row_personel:
                ttd_kiri = ''
                ttd_kanan = ''
                if (no_personel % 2) == 0:
                    ttd_kanan = str(no_personel) + '. .................'
                else:
                    ttd_kiri = str(no_personel) + '. .................'
                line_personel = {
                    'jabatan':recpersonel[0] if recpersonel[0] else "",
                    'satwa':recpersonel[1] if recpersonel[1] else "",
                    'pangkat':recpersonel[2] if recpersonel[2] else "",
                    'keterangan':recpersonel[3] if recpersonel[3] else "",
                    'nama':recpersonel[4] if recpersonel[4] else "",
                    'nrp':recpersonel[5] if recpersonel[5] else "",
                    'no': str(no_personel) + '.',
                    'hari':1,
                    'ttd_kiri':ttd_kiri,
                    'ttd_kanan':ttd_kanan,
                }
                no_personel += 1
                arrpersonel.append(line_personel)
            context['personel'] = arrpersonel
            curpersonel.close()
            tot_dana = 5000 * len(arrpersonel)
            tot_terima = 47000 * len(arrpersonel)
            context['tot_dana'] = "{:,}".format(tot_dana).replace(',','.')
            context['tot_terima'] = "{:,}".format(tot_terima).replace(',','.')

    except psycopg2.Error  as error:
        respdata.append("ERROR")
        respdata.append(error.pgerror)
        return respdata
    finally:
        if(connection):
            connection.close()


    doc = DocxTemplate(path +"/PATROLI_WABKU.docx")

    doc.render(context)

    filename = "PATROLI_WABKU_" + grup_id.replace('/','_').replace('\\','_') + ".docx"
    filepath = result_path + "/" + filename
    doc.save(filepath)

    doc_json = dict()
    encoded_content = helper.file_to_base64(filepath)
    doc_json["file_name"] = filename
    doc_json["mime"] = "application/octet-stream"
    doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")

    return doc_json
    
def createdoc_patroli_absen(grup_id:str):


    context = { }

    engine = db.engine
    connection = engine.raw_connection();

    respdata = []
    message = ''
    blankdot = '..............................................................'
    id_sprin_cakum = ''
    nama_hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu']
    nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    nama_romawi = ['I','II','III','IV','V']

    row_personel = []

    tanggal_now = datetime.now()
    tanggal = str(tanggal_now.day)
    bulan = nama_bulan[tanggal_now.month - 1]
    tahun = str(tanggal_now.year)
    tanggal_cetak = tanggal + "-" + bulan + "-" + tahun
    context['tanggal_cetak'] = tanggal_cetak

    try:
        curhead = connection.cursor()
        strSQL = "SELECT a.id, a.no_sprin, a.tanggal_sprin, a.tugas_awal, a.tugas_akhir,a.jam_tugas"
        strSQL += " ,b.wilkum_area, b.tanggal_patroli"
        strSQL += " ,c.route, c.tugas, c.saran, c.penutup"
        strSQL += " ,d.polres, c.created_date"
        strSQL += " FROM sprin_patroli a INNER JOIN sprin_patroli_grup b ON a.id = b.sprin_patroli_id"
        strSQL += " INNER JOIN sprin_patroli_laporan c ON b.id = c.grup_id"
        strSQL += " INNER JOIN polres d ON d.id = b.wilkum_id"
        strSQL += " WHERE b.id = '" + str(grup_id) + "'"
        curhead.execute(strSQL)
        rechead = curhead.fetchone()
        if rechead:
            sprin_patroli_id = rechead[0]
            no_sprin = rechead[1]
            tanggal_sprin_date = rechead[2]
            tugas_awal = rechead[3]
            tugas_akhir = rechead[4]
            jam_tugas = rechead[5]
            wilkum_area = rechead[6]
            tanggal_patroli = rechead[7]
            route = rechead[8]
            tugas = rechead[9]
            saran = rechead[10]
            penutup = rechead[11]
            polres = rechead[12]
            created_date = rechead[13]

            context['no_sprin'] = no_sprin

            hari = nama_hari[tanggal_patroli.weekday()]
            tanggal = tanggal_patroli.day
            bulan = tanggal_patroli.month
            tahun = tanggal_patroli.year

            context['tanggal_patroli'] = str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)

            context['tanggal_sprin'] = str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)
            context['jam_tugas'] = jam_tugas
            context['polres'] = polres

            hari = nama_hari[created_date.weekday()]
            tanggal = created_date.day
            bulan = created_date.month
            tahun = created_date.year

            context['created_date'] = hari + ', ' + str(tanggal) + ' ' + nama_bulan[bulan-1] + ' ' + str(tahun)

            curpersonel = connection.cursor()
            strSQL = "SELECT b.jabatan, c.nama_satwa, d.pangkat, a.keterangan, e.full_name,e.nrp"
            strSQL += " FROM sprin_patroli_personel a LEFT OUTER JOIN jabatan b on a.jabatan_id = b.id"
            strSQL += " LEFT OUTER JOIN asset_satwa c ON a.satwa_id = c.id"
            strSQL += " LEFT OUTER JOIN pangkat d ON a.pangkat_id = d.id"
            strSQL += " LEFT OUTER JOIN user_profiles e ON a.profile_id = e.id"
            strSQL += " WHERE a.grup_id = '" + str(grup_id) + "' AND sprin_patroli_id = '" + str(sprin_patroli_id) + "'"
            curpersonel.execute(strSQL)
            row_personel = curpersonel.fetchall()
            arrpersonel = []
            no_personel = 1
            
            for recpersonel in row_personel:
                ttd_kiri = ''
                ttd_kanan = ''
                if (no_personel % 2) == 0:
                    ttd_kanan = str(no_personel) + '. .................'
                else:
                    ttd_kiri = str(no_personel) + '. .................'
                line_personel = {
                    'jabatan':recpersonel[0] if recpersonel[0] else "",
                    'satwa':recpersonel[1] if recpersonel[1] else "",
                    'pangkat':recpersonel[2] if recpersonel[2] else "",
                    'keterangan':recpersonel[3] if recpersonel[3] else "",
                    'nama':recpersonel[4] if recpersonel[4] else "",
                    'nrp':recpersonel[5] if recpersonel[5] else "",
                    'no': str(no_personel) + '.',
                    'hari':1,
                    'ttd_kiri':ttd_kiri,
                    'ttd_kanan':ttd_kanan,
                }
                no_personel += 1
                arrpersonel.append(line_personel)
            context['personel'] = arrpersonel
            curpersonel.close()
            tot_dana = 5000 * len(arrpersonel)
            tot_terima = 47000 * len(arrpersonel)
            context['tot_dana'] = "{:,}".format(tot_dana).replace(',','.')
            context['tot_terima'] = "{:,}".format(tot_terima).replace(',','.')

    except psycopg2.Error  as error:
        respdata.append("ERROR")
        respdata.append(error.pgerror)
        return respdata
    finally:
        if(connection):
            connection.close()

    doc = DocxTemplate(path +"/PATROLI_ABSEN.docx")

    doc.render(context)

    filename = "PATROLI_ABSEN_" + grup_id.replace('/','_').replace('\\','_') + ".docx"
    filepath = result_path + "/" + filename
    doc.save(filepath)

    doc_json = dict()
    encoded_content = helper.file_to_base64(filepath)
    doc_json["file_name"] = filename
    doc_json["mime"] = "application/octet-stream"
    doc_json["content"] = "data:" + doc_json["mime"] + ";base64," + encoded_content.decode("ascii")

    return doc_json

