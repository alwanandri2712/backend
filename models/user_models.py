# Copyright 2014 SolidBuilds.com. All rights reserved
#
# Authors: Ling Thio <ling.thio@gmail.com>, Matt Hogan <matt@twintechlabs.io>

from flask_user import UserMixin
from flask_user.forms import RegisterForm
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, validators
from app import db
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, text
from sqlalchemy.dialects.postgresql import TIMESTAMP, UUID

class AuditMixin(object):
    deleted = db.Column(Boolean)
    created_by = db.Column(String(50))
    created_date = db.Column(DateTime)
    updated_by = db.Column(String(50))
    updated_date = db.Column(DateTime)


# Define the User data model. Make sure to add the flask_user.UserMixin !!
class User(db.Model, UserMixin,AuditMixin):
    __tablename__ = 'users'
    id = db.Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    username =  db.Column(db.String(100), nullable=False, server_default='')
    email = db.Column(db.Unicode(100), nullable=False, server_default=u'', unique=True)
    password = db.Column(db.String(100), nullable=False, server_default='')
    is_active = db.Column('is_active', db.Boolean(), nullable=False, server_default='0')
    first_name =  db.Column(db.String(100), nullable=False, server_default='')
    last_name =  db.Column(db.String(100), nullable=False, server_default='')
    # Relationships
    roles = db.relationship('Role', secondary='users_roles',
                            primaryjoin="and_(UsersRoles.user_id == User.id,UsersRoles.deleted == False)",
                            backref=db.backref('users', lazy='dynamic'))
    def has_role(self, role):
        for item in self.roles:
            if item.name == 'admin':
                return True
        return False

    def role(self):
        #print(self.roles)
        items = []
        for item in self.roles:
            items.append(item.name)
        return items

    def name(self):
        return self.first_name + " " + self.last_name


# Define the Role data model
class Role(db.Model,AuditMixin):
    __tablename__ = 'roles'
    id = db.Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    name = db.Column(db.String(50), nullable=False, server_default=u'', unique=True)  # for @roles_accepted()
    label = db.Column(db.Unicode(255), server_default=u'')  # for display purposes


# Define the UserRoles association model
class UsersRoles(db.Model,AuditMixin):
    __tablename__ = 'users_roles'
    id = db.Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    user_id = db.Column(UUID, db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(UUID, db.ForeignKey('roles.id', ondelete='RESTRICT'))


# Define the User registration form
# It augments the Flask-User RegisterForm with additional fields
class MyRegisterForm(RegisterForm):
    first_name = StringField('First name', validators=[
        validators.DataRequired('First name is required')])
    last_name = StringField('Last name', validators=[
        validators.DataRequired('Last name is required')])


# Define the User profile form
class UserProfileForm(FlaskForm):
    first_name = StringField('First name', validators=[
        validators.DataRequired('First name is required')])
    last_name = StringField('Last name', validators=[
        validators.DataRequired('Last name is required')])
    submit = SubmitField('Save')
