# coding: utf-8
from sqlalchemy import Boolean, Column, Date, DateTime, ForeignKey, String, text
from sqlalchemy.dialects.postgresql import TIMESTAMP, UUID
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Golongan(Base):
    __tablename__ = 'golongan'

    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    kode_golongan = Column(String(10))
    nama_golongan = Column(String(50))
    keterangan = Column(String(255))
    created_by = Column(String(100))
    created_date = Column(TIMESTAMP(precision=6))
    updated_by = Column(String(100))
    updated_date = Column(TIMESTAMP(precision=6))
    deleted = Column(Boolean, server_default=text("false"))


class Kelurahan(Base):
    __tablename__ = 'kelurahan'

    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    id_zona = Column(UUID)
    kode_kelurahan = Column(String(10))
    nama_kelurahan = Column(String(255))
    created_by = Column(String(100))
    created_date = Column(TIMESTAMP(precision=6))
    updated_by = Column(String(100))
    updated_date = Column(TIMESTAMP(precision=6))
    deleted = Column(Boolean, server_default=text("false"))


class Pelanggan(Base):
    __tablename__ = 'pelanggan'

    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    kode_pelanggan = Column(String(20))
    nama_pelanggan = Column(String(100))
    alamat = Column(String(255))
    telp = Column(String(15))
    mobile = Column(String(15))
    fax = Column(String(15))
    no_ktp = Column(String(100))
    email = Column(String(100))
    status = Column(String(30))
    created_by = Column(String(50))
    created_date = Column(DateTime)
    updated_by = Column(String(50))
    updated_date = Column(DateTime)
    deleted = Column(Boolean, server_default=text("false"))


class Role(Base):
    __tablename__ = 'roles'

    deleted = Column(Boolean)
    created_by = Column(String(50))
    created_date = Column(DateTime)
    updated_by = Column(String(50))
    updated_date = Column(DateTime)
    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    name = Column(String(50), nullable=False, unique=True, server_default=text("''::character varying"))
    label = Column(String(255), server_default=text("''::character varying"))


class Rt(Base):
    __tablename__ = 'rt'

    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    kode_rt = Column(String(10))
    nama_rt = Column(String(100))
    keterangan = Column(String(255))
    created_by = Column(String(100))
    created_date = Column(TIMESTAMP(precision=6))
    updated_by = Column(String(100))
    updated_date = Column(TIMESTAMP(precision=6))
    deleted = Column(Boolean, server_default=text("false"))


class Rw(Base):
    __tablename__ = 'rw'

    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    kode_rw = Column(String(10))
    nama_rw = Column(String(100))
    created_by = Column(String(100))
    created_date = Column(TIMESTAMP(precision=6))
    updated_by = Column(String(100))
    updated_date = Column(TIMESTAMP(precision=6))
    keterangan = Column(String(255))
    deleted = Column(Boolean, server_default=text("false"))


class User(Base):
    __tablename__ = 'users'

    deleted = Column(Boolean)
    created_by = Column(String(50))
    created_date = Column(DateTime)
    updated_by = Column(String(50))
    updated_date = Column(DateTime)
    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    username = Column(String(100), nullable=False, server_default=text("''::character varying"))
    email = Column(String(100), nullable=False, unique=True, server_default=text("''::character varying"))
    password = Column(String(100), nullable=False, server_default=text("''::character varying"))
    is_active = Column(Boolean, nullable=False, server_default=text("false"))
    first_name = Column(String(100), nullable=False, server_default=text("''::character varying"))
    last_name = Column(String(100), nullable=False, server_default=text("''::character varying"))


class Zona(Base):
    __tablename__ = 'zona'

    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    kode_zona = Column(String(10))
    nama_zona = Column(String(100))
    keterangan = Column(String(255))
    created_by = Column(String(100))
    created_date = Column(TIMESTAMP(precision=6))
    update_by = Column(String(100))
    update_date = Column(TIMESTAMP(precision=6))
    deleted = Column(Boolean, server_default=text("false"))


class Sambung(Base):
    __tablename__ = 'sambung'

    id = Column(UUID, primary_key=True)
    id_pelanggan = Column(ForeignKey('pelanggan.id', ondelete='RESTRICT', onupdate='CASCADE'))
    id_golongan = Column(ForeignKey('golongan.id', ondelete='RESTRICT', onupdate='CASCADE'))
    id_zona = Column(ForeignKey('zona.id', ondelete='RESTRICT', onupdate='CASCADE'))
    id_kelurahan = Column(ForeignKey('kelurahan.id', ondelete='RESTRICT', onupdate='CASCADE'))
    id_rw = Column(ForeignKey('rw.id', ondelete='RESTRICT', onupdate='CASCADE'))
    id_rt = Column(ForeignKey('rt.id', ondelete='RESTRICT', onupdate='CASCADE'))
    no_rumah = Column(String(4))
    no_sambung = Column(String(20))
    tanggal_daftar = Column(Date)
    tanggal_pasang = Column(Date)
    bulan_pasang = Column(String(2))
    tahun_pasang = Column(String(4))
    latitude = Column(String(255))
    longitude = Column(String(255))
    status = Column(String(30))
    created_by = Column(String(100))
    created_date = Column(DateTime)
    updated_by = Column(String(100))
    updated_date = Column(DateTime)
    deleted = Column(Boolean, server_default=text("false"))

    golongan = relationship('Golongan')
    kelurahan = relationship('Kelurahan')
    pelanggan = relationship('Pelanggan')
    rt = relationship('Rt')
    rw = relationship('Rw')
    zona = relationship('Zona')


class UsersRole(Base):
    __tablename__ = 'users_roles'

    deleted = Column(Boolean)
    created_by = Column(String(50))
    created_date = Column(DateTime)
    updated_by = Column(String(50))
    updated_date = Column(DateTime)
    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    user_id = Column(ForeignKey('users.id', ondelete='CASCADE'))
    role_id = Column(ForeignKey('roles.id', ondelete='RESTRICT'))

    role = relationship('Role')
    user = relationship('User')
