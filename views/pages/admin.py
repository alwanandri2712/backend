# Copyright 2017 Twin Tech Labs. All rights reserved

from flask import Blueprint, redirect, render_template, current_app
from libs.base_module import HandlerBase
from local_settings import MODE


class PortalPageView(HandlerBase):

    def get(self):
        #redirect to admin page for a while
        if MODE == 'development':
            return render_template('admin_dev.html')
        else:
            return render_template('admin_prod.html')