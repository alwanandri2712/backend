import datetime
from flask import request, jsonify, render_template
from flask_wtf import FlaskForm
from wtforms import StringField,BooleanField,FloatField,IntegerField,DateField
from wtforms.validators import DataRequired
import simplejson

from libs.auth_jwt import authorize_roles
from libs.base_module import  HandlerBase
import requests
from helpers.pagination import abbreviated_pages

from models.entities import User
from services.UserService import UserService

class UserForm(FlaskForm):
    id = StringField("id")
    email = StringField("email",validators=[DataRequired("This field is required.")])
    username = StringField("username",validators=[DataRequired("This field is required.")])
    password = StringField("password")
    is_active = BooleanField("is_active")
    first_name = StringField("first_name",validators=[DataRequired("This field is required.")])
    last_name = StringField("last_name",validators=[DataRequired("This field is required.")])
    


class UserBaseApi(HandlerBase):

    def __init__(self):
        HandlerBase.__init__(self)
        self.user_service = UserService()

    @authorize_roles("user")
    def get(self):
        success = True
        rows = []
        count = 0
        message = ""
        strfilter = request.args.get("filter")
        strsorter = request.args.get("sort")
        page = int(request.args.get("page")) if request.args.get("page") is not None else 1
        page_size = int(request.args.get("page_size")) if request.args.get("page_size") else self.default_page_size
        filter = []
        sort = []
        page_numbers = []
        if strfilter:
            filter = simplejson.loads(strfilter)
        if strsorter:
            sort = simplejson.loads(strsorter)
        try:
            res = self.user_service.find(filter, sort, page, page_size)
            rows = res.get("rows")
            count = res.get("count")
            page_numbers = abbreviated_pages(page_size, count, page)
        except Exception as e:
            success = False
            message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)
            self.write_log(e)

        ret = dict(success=success, rows=rows, total=count, message=message,page_numbers=page_numbers)
        return jsonify(ret), 200

    @authorize_roles("user")
    def post(self):
        success = True
        message = "Simpan Data Berhasil"
        user_form = UserForm()

        data = user_form.data
        if not user_form.validate_on_submit():
            success = False
            message = "Form Tidak Valid"
        else:
            try:
                data["created_date"] = datetime.datetime.now()
                data["created_by"] = self.get_current_user()
                created = self.user_service.create(data)
                data["id"] = created
            except Exception as e:
                success = False
                self.write_log(e)
                message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data, errors=user_form.errors)
        return jsonify(ret), 200

    @authorize_roles("user")
    def put(self):
        success = True
        message = "Ubah Data Berhasil"
        user_form = UserForm()

        data = user_form.data
        if not user_form.validate_on_submit():
            success = False
            message = "Form Tidak Valid"
        else:
            try:
                data["updated_date"] = datetime.datetime.now()
                data["updated_by"] = self.get_current_user()
                self.user_service.update(data)
            except Exception as e:
                success = False
                self.write_log(e)
                message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data, errors=user_form.errors)
        return jsonify(ret), 200


class UserDetailApi(HandlerBase):

    def __init__(self):
        HandlerBase.__init__(self)
        self.user_service = UserService()

    @authorize_roles("user")
    def get(self,id):
        success = True
        data = {}
        count = 0
        message = ""
        try:
            data = self.user_service.find_by_pk(id)
        except Exception as e:
            success = False
            message = str(e)
            message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)
            self.write_log(e)
        ret = dict(success=success, data=data, count=count, message=message)
        return jsonify(ret), 200

    @authorize_roles("user")
    def delete(self,id):
        success = True
        message = "Hapus Data Berhasil"
        try:
            self.user_service.delete(id, self.get_current_user())
        except Exception as e:
            success = False
            self.write_log(e)
            message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)

        ret = dict(message=message, success=success)
        return jsonify(ret), 200