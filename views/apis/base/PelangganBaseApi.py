import datetime
from flask import request, jsonify, render_template
from flask_wtf import FlaskForm
from wtforms import StringField,BooleanField,FloatField,IntegerField,DateField
from wtforms.validators import DataRequired
import simplejson

from libs.auth_jwt import authorize_roles
from libs.base_module import  HandlerBase
import requests
from helpers.pagination import abbreviated_pages

from models.entities import Pelanggan
from services.PelangganService import PelangganService

class PelangganForm(FlaskForm):
    id = StringField("id")
    kode_pelanggan = StringField("kode_pelanggan")
    nama_pelanggan = StringField("nama_pelanggan")
    alamat = StringField("alamat")
    telp = StringField("telp")
    mobile = StringField("mobile")
    fax = StringField("fax")
    no_ktp = StringField("no_ktp")
    email = StringField("email")
    status = StringField("status")
    created_by = StringField("created_by")
    created_date = DateField("created_date")
    updated_by = StringField("updated_by")
    updated_date = DateField("updated_date")
    deleted = BooleanField("deleted")
    


class PelangganBaseApi(HandlerBase):

    def __init__(self):
        HandlerBase.__init__(self)
        self.pelanggan_service = PelangganService()

    @authorize_roles("user")
    def get(self):
        success = True
        rows = []
        count = 0
        message = ""
        strfilter = request.args.get("filter")
        strsorter = request.args.get("sort")
        page = int(request.args.get("page")) if request.args.get("page") is not None else 1
        page_size = int(request.args.get("page_size")) if request.args.get("page_size") else self.default_page_size
        filter = []
        sort = []
        page_numbers = []
        if strfilter:
            filter = simplejson.loads(strfilter)
        if strsorter:
            sort = simplejson.loads(strsorter)
        try:
            res = self.pelanggan_service.find(filter, sort, page, page_size)
            rows = res.get("rows")
            count = res.get("count")
            page_numbers = abbreviated_pages(page_size, count, page)
        except Exception as e:
            success = False
            message = "System error, please contact IT support. Error: " + str(e)
            self.write_log(e)

        ret = dict(success=success, rows=rows, total=count, message=message,page_numbers=page_numbers)
        return jsonify(ret), 200

    @authorize_roles("user")
    def post(self):
        success = True
        message = "Create success"
        pelanggan_form = PelangganForm()

        data = pelanggan_form.data
        if not pelanggan_form.validate_on_submit():
            success = False
            message = "Form is not valid"
        else:
            try:
                data["created_date"] = datetime.datetime.now()
                data["created_by"] = self.get_current_user()
                created = self.pelanggan_service.create(data)
                data["id"] = created
            except Exception as e:
                success = False
                self.write_log(e)
                message = "System error, please contact IT support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data, errors=pelanggan_form.errors)
        return jsonify(ret), 200

    @authorize_roles("user")
    def put(self):
        success = True
        message = "Update success"
        pelanggan_form = PelangganForm()

        data = pelanggan_form.data
        if not pelanggan_form.validate_on_submit():
            success = False
            message = "Form is not valid"
        else:
            try:
                data["updated_date"] = datetime.datetime.now()
                data["updated_by"] = self.get_current_user()
                self.pelanggan_service.update(data)
            except Exception as e:
                success = False
                self.write_log(e)
                message = "System error, please contact IT support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data, errors=pelanggan_form.errors)
        return jsonify(ret), 200


class PelangganDetailApi(HandlerBase):

    def __init__(self):
        HandlerBase.__init__(self)
        self.pelanggan_service = PelangganService()

    @authorize_roles("user")
    def get(self,id):
        success = True
        data = {}
        count = 0
        message = ""
        try:
            data = self.pelanggan_service.find_by_pk(id)
        except Exception as e:
            success = False
            message = str(e)
            message = "System error, please contact IT support. Error: " + str(e)
            self.write_log(e)
        ret = dict(success=success, data=data, count=count, message=message)
        return jsonify(ret), 200

    @authorize_roles("user")
    def delete(self,id):
        success = True
        message = "Delete success"
        try:
            self.pelanggan_service.delete(id, self.get_current_user())
        except Exception as e:
            success = False
            self.write_log(e)
            message = "System error, please contact IT support. Error: " + str(e)

        ret = dict(message=message, success=success)
        return jsonify(ret), 200