

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.UsersRoleService import UsersRoleService
from views.apis.base.UsersRoleBaseApi import UsersRoleForm

class UsersRoleApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,UsersRoleService(),UsersRoleForm)


class UsersRoleDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,UsersRoleService(),UsersRoleForm)