

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.RwService import RwService
from views.apis.base.RwBaseApi import RwForm

class RwApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,RwService(),RwForm)


class RwDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,RwService(),RwForm)


'''
Copy this to app/routes.py :

from views.apis.RwApi import RwApi,RwDetailApi

self.app.add_url_rule("/api/v1/rw", methods=["GET", "POST", "PUT"],view_func=RwApi.as_view("rw_api"))
self.app.add_url_rule("/api/v1/rw/<id>", methods=["GET", "DELETE"],view_func=RwDetailApi.as_view("rw_detail_api"))

'''
