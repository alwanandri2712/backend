import flask_jwt_extended

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.UserService import UserService
from views.apis.base.UserBaseApi import UserForm
import datetime
from flask import request, jsonify, render_template, current_app
from flask_wtf import FlaskForm
from wtforms import StringField,BooleanField,FloatField,IntegerField,DateField
from wtforms.validators import DataRequired, EqualTo, ValidationError
import simplejson

from libs.auth_jwt import authorize_roles
from libs.base_module import  HandlerBase
import requests
from helpers.pagination import abbreviated_pages

from models.entities import User
from services.UserService import UserService


class UserApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,UserService(),UserForm)

    def post(self):
        success = True
        message = "Simpan Data Berhasil"
        user_form = UserForm()

        data = user_form.data
        if not user_form.validate_on_submit():
            success = False
            message = "Form tidak valid"
        else:
            try:
                data["password"] = current_app.user_manager.hash_password("user123") #give default password , shall we?
                data["created_date"] = datetime.datetime.now()
                data["created_by"] = self.get_current_user()
                created = self.service.create(data)
                data["id"] = created
            except Exception as e:
                success = False
                self.write_log(e)
                message = "Sistem error, silahkan kontak IT support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data, errors=user_form.errors)
        return jsonify(ret), 200
    
    @authorize_roles("user")
    def get(self):
        success = True
        rows = []
        count = 0
        message = ""
        strfilter = request.args.get("filter")
        strsorter = request.args.get("sort")
        page = int(request.args.get("page")) if request.args.get("page") is not None else 1
        page_size = int(request.args.get("page_size")) if request.args.get("page_size") else self.default_page_size
        filter = []
        sort = []
        page_numbers = []
        if strfilter:
            filter = simplejson.loads(strfilter)
        if strsorter:
            sort = simplejson.loads(strsorter)
        try:
            filter.append({
                "property" : "username",
                "value" : "admin",
                "operator" : "like"
            })
            res = self.service.find(filter, sort, page, page_size)
            rows = res.get("rows")
            count = res.get("count")
            page_numbers = abbreviated_pages(page_size, count, page)
        except Exception as e:
            success = False
            message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)
            self.write_log(e)

        ret = dict(success=success, rows=rows, total=count, message=message,page_numbers=page_numbers)
        return jsonify(ret), 200
    
    
    
class UserDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,UserService(),UserForm)


class UserPasswordResetter(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self, UserService(), UserForm)
        self.service = UserService()

    @authorize_roles("user")
    def put(self):
        success = True
        message = "Update success"
        try:
            data = {}
            data["new_password"] = current_app.user_manager.hash_password("user123")  # give default password , shall we?
            data["updated_date"] = datetime.datetime.now()
            data["updated_by"] = self.get_current_user()
            self.service.reset_password(data)
        except Exception as e:
            success = False
            self.write_log(e)
            message = "Sistem error, silahkan kontak IT support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data)
        return jsonify(ret), 200


class ChangePasswordForm(FlaskForm):
    current_password = StringField("current_password",validators=[DataRequired("Current password is required.")])
    new_password = StringField("new_password",[DataRequired("New password is required."),EqualTo('confirm_password', message='Passwords must match')])
    confirm_password = StringField("confirm_password",[DataRequired("Confirm password is required.")])

    def validate_current_password(form, field):
        #current_password_hash = current_app.user_manager.hash_password(field.data)
        user_service = UserService()
        current_username = flask_jwt_extended.get_jwt_identity()
        user = None
        users = user_service.find([{
            "property" : "email",
            "value" : current_username
        }],[])
        is_password_match = False
        if len(users.get("rows")) > 0:
            user = users.get("rows")[0]
        if user is not None:
            is_password_match = current_app.user_manager.verify_password(field.data,user.get("password"))
        else:
            raise  ValidationError("No user found.")
        if not is_password_match:
            raise ValidationError('Wrong current password')


class UserPasswordChanger(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self, UserService(), UserForm)
        self.service = UserService()

    @authorize_roles("user")
    def put(self):
        success = True
        message = "Update success"
        user_form = ChangePasswordForm()

        data = user_form.data
        if not user_form.validate_on_submit():
            success = False
            message = "Form tidak valid"
        else:
            try:
                data["new_password"] = current_app.user_manager.hash_password(data["new_password"])
                data["updated_date"] = datetime.datetime.now()
                data["updated_by"] = self.get_current_user()
                self.service.reset_password(data)
            except Exception as e:
                success = False
                self.write_log(e)
                message = "Sistem error, silahkan kontak IT support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data, errors=user_form.errors)
        return jsonify(ret), 200




