# Copyright 2018 Twin Tech Labs. All rights reserved
import simplejson
from flask import Blueprint
from flask import request,  jsonify
from flask_jwt_extended import jwt_required, create_access_token,create_refresh_token, jwt_refresh_token_required, get_jwt_identity
from flask_user import current_app
from wtforms import Form
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
import base64

import subprocess
from app import db
from models.user_models import User

from libs.base_module import HandlerBase

# When using a Flask app factory we must use a blueprint to avoid needing 'app' for '@app.route'
from libs.auth_jwt import get_roles, authorize_roles


class SampleApi(HandlerBase):

    @authorize_roles("admin")
    def get(self):
        self.get_current_roles()
        ret = {"message": "Hello {0}, good night!".format(self.get_current_user())}
        return(jsonify(ret), 200)



class LoginApi(HandlerBase):

    def post(self):
        ret = {
            "success" : False,
            "message" : "Login Gagal, username atau password salah"
        }
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"}), 400

        username = request.json.get('username', None)
        password = request.json.get('password', None)
        if not username or not password:
            return jsonify(ret), 200

        try:

            user  :User = db.session.query(User)\
                        .filter(User.username == username)\
                        .filter(User.deleted == False)\
                        .first()

            if user is None:
                return jsonify(ret), 200

            password_verified = current_app.user_manager.verify_password(password,user.password)
            if not password_verified:
                return jsonify(ret), 200

            user_claims = {
                "user_id" : user.id,
                "full_name" : user.first_name + " " + user.last_name,
                "roles" : get_roles(user)
            }

            # Identity can be any data that is json serializable
            #access_token = create_access_token(identity=username,user_claims=user_claims)
            ret = {
                'success' : True,
                'message' : 'Login Berhasil',
                'access_token': create_access_token(identity=username,user_claims=user_claims),
                'refresh_token': create_refresh_token(identity=username,user_claims=user_claims)
            }
        except Exception as e:
            ret = {
                'success': False,
                'message': 'Sistem Error , ' + str(e)
            }

        return jsonify(ret), 200



class TokenApi(HandlerBase):

    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        user = db.session.query(User).filter(User.email == current_user).one()
        user_claims = {
            "roles": get_roles(user)
        }
        ret = {
            'access_token': create_access_token(identity=current_user,user_claims=user_claims)
        }
        return jsonify(ret), 200


class PrintForm(FlaskForm):
    html_content = StringField("Html Content",validators=[DataRequired("Content is missing")])


class PrintApi(HandlerBase):

    def post(self):
        success = True
        message = "Print Berhasil"
        model_form = PrintForm()
        data = model_form.data
        if not model_form.validate_on_submit():
            success = False
            message = "Form tidak valid"
        else:
            try:
                f = open("./app/uploads/report.html","w+")
                f.write(data.get("html_content"))
                f.close()
                #pandoc test1.md -f markdown -t html -s -o test1.html
                filename = "report.docx"
                subprocess.call(["pandoc", "./app/uploads/report.html", "-f","html","--standalone","-o","./app/uploads/" + filename,"--pdf-engine=wkhtmltopdf"])
                f = open("./app/uploads/" + filename,"rb")
                content = f.read()

                encoded_content = base64.b64encode(content)
                f.close()
                data["file_name"] = filename
                data["mime"] = "application/octet-stream"
                data["content"] = "data:"+data["mime"]+";base64," + encoded_content.decode("ascii")
            except Exception as e:
                success = False
                self.write_log(e)
                message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)

        ret = dict(message=message, success=success, data=data, errors=model_form.errors)
        return jsonify(ret), 200