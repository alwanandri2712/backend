

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.SambungService import SambungService
from views.apis.base.SambungBaseApi import SambungForm

class SambungApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,SambungService(),SambungForm)


class SambungDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,SambungService(),SambungForm)


'''
Copy this to app/routes.py :

from views.apis.SambungApi import SambungApi,SambungDetailApi

self.app.add_url_rule("/api/v1/sambung", methods=["GET", "POST", "PUT"],view_func=SambungApi.as_view("sambung_api"))
self.app.add_url_rule("/api/v1/sambung/<id>", methods=["GET", "DELETE"],view_func=SambungDetailApi.as_view("sambung_detail_api"))

'''
