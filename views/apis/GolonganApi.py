

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.GolonganService import GolonganService
from views.apis.base.GolonganBaseApi import GolonganForm

class GolonganApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,GolonganService(),GolonganForm)


class GolonganDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,GolonganService(),GolonganForm)


'''
Copy this to app/routes.py :

from views.apis.GolonganApi import GolonganApi,GolonganDetailApi

self.app.add_url_rule("/api/v1/golongan", methods=["GET", "POST", "PUT"],view_func=GolonganApi.as_view("golongan_api"))
self.app.add_url_rule("/api/v1/golongan/<id>", methods=["GET", "DELETE"],view_func=GolonganDetailApi.as_view("golongan_detail_api"))

'''
