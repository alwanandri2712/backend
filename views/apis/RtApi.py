

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.RtService import RtService
from views.apis.base.RtBaseApi import RtForm

class RtApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,RtService(),RtForm)


class RtDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,RtService(),RtForm)


'''
Copy this to app/routes.py :

from views.apis.RtApi import RtApi,RtDetailApi

self.app.add_url_rule("/api/v1/rt", methods=["GET", "POST", "PUT"],view_func=RtApi.as_view("rt_api"))
self.app.add_url_rule("/api/v1/rt/<id>", methods=["GET", "DELETE"],view_func=RtDetailApi.as_view("rt_detail_api"))

'''
