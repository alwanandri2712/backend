

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.ZonaService import ZonaService
from views.apis.base.ZonaBaseApi import ZonaForm

class ZonaApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,ZonaService(),ZonaForm)


class ZonaDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,ZonaService(),ZonaForm)


'''
Copy this to app/routes.py :

from views.apis.ZonaApi import ZonaApi,ZonaDetailApi

self.app.add_url_rule("/api/v1/zona", methods=["GET", "POST", "PUT"],view_func=ZonaApi.as_view("zona_api"))
self.app.add_url_rule("/api/v1/zona/<id>", methods=["GET", "DELETE"],view_func=ZonaDetailApi.as_view("zona_detail_api"))

'''
