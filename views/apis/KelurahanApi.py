

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.KelurahanService import KelurahanService
from views.apis.base.KelurahanBaseApi import KelurahanForm

class KelurahanApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,KelurahanService(),KelurahanForm)


class KelurahanDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,KelurahanService(),KelurahanForm)


'''
Copy this to app/routes.py :

from views.apis.KelurahanApi import KelurahanApi,KelurahanDetailApi

self.app.add_url_rule("/api/v1/kelurahan", methods=["GET", "POST", "PUT"],view_func=KelurahanApi.as_view("kelurahan_api"))
self.app.add_url_rule("/api/v1/kelurahan/<id>", methods=["GET", "DELETE"],view_func=KelurahanDetailApi.as_view("kelurahan_detail_api"))

'''
