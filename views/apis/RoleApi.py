

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.RoleService import RoleService
from views.apis.base.RoleBaseApi import RoleForm


class RoleApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,RoleService(),RoleForm)


class RoleDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,RoleService(),RoleForm)