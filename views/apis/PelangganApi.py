

from libs.base_module import HandleBaseEntityMixin,HandleBaseEntityDetailMixin
from services.PelangganService import PelangganService
from views.apis.base.PelangganBaseApi import PelangganForm

class PelangganApi(HandleBaseEntityMixin):

    def __init__(self):
        HandleBaseEntityMixin.__init__(self,PelangganService(),PelangganForm)


class PelangganDetailApi(HandleBaseEntityDetailMixin):

    def __init__(self):
        HandleBaseEntityDetailMixin.__init__(self,PelangganService(),PelangganForm)


'''
Copy this to app/routes.py :

from views.apis.PelangganApi import PelangganApi,PelangganDetailApi

self.app.add_url_rule("/api/v1/pelanggan", methods=["GET", "POST", "PUT"],view_func=PelangganApi.as_view("pelanggan_api"))
self.app.add_url_rule("/api/v1/pelanggan/<id>", methods=["GET", "DELETE"],view_func=PelangganDetailApi.as_view("pelanggan_detail_api"))

'''
