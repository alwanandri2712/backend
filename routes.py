
# views
from views.pages.admin import PortalPageView


# apis
from views.apis.apis import LoginApi, SampleApi, TokenApi, PrintApi
from views.apis.RoleApi import RoleApi, RoleDetailApi
from views.apis.UserApi import UserApi, UserDetailApi, UserPasswordResetter, UserPasswordChanger
from views.apis.UsersRoleApi import UsersRoleApi, UsersRoleDetailApi
from views.apis.RtApi import RtApi,RtDetailApi
from views.apis.ZonaApi import ZonaApi,ZonaDetailApi
from views.apis.GolonganApi import GolonganApi,GolonganDetailApi
from views.apis.RwApi import RwApi,RwDetailApi
from views.apis.PelangganApi import PelangganApi,PelangganDetailApi
from views.apis.KelurahanApi import KelurahanApi,KelurahanDetailApi
from views.apis.SambungApi import SambungApi,SambungDetailApi




class Router():

    def __init__(self, app):
        self.app = app
        self.base_api_url = '/api/v1'
        self.base_page_url = '/page/v1'

    def _verbose(self, url):
        print("Registering route " + url)

    def _generate_url(self, path, type="page"):
        url = "{0}/{1}".format(self.base_page_url, path)
        if type == "api":
            url = "{0}/{1}".format(self.base_api_url, path)

        print(url)
        return url

    def register_view(self):
        # register views
        self.app.add_url_rule("/", methods=["GET", ], view_func=PortalPageView.as_view("portal_view"))

        # register api
        self.app.add_url_rule(self._generate_url("login", "api"), methods=["POST", ], view_func=LoginApi.as_view("login_api"))
        self.app.add_url_rule(self._generate_url("print", "api"), methods=["POST", ], view_func=PrintApi.as_view("print_api"))

        self.app.add_url_rule(self._generate_url("token/refresh", "api"), methods=["POST", ], view_func=TokenApi.as_view("token_api"))
        self.app.add_url_rule(self._generate_url("sample", "api"), methods=["GET", ], view_func=SampleApi.as_view("sample_api"))

        self.app.add_url_rule("/api/v1/role", methods=["GET", "POST", "PUT"], view_func=RoleApi.as_view("role_api"))
        self.app.add_url_rule("/api/v1/role/<id>", methods=["GET", "DELETE"], view_func=RoleDetailApi.as_view("role_detail_api"))

        self.app.add_url_rule("/api/v1/user", methods=["GET", "POST", "PUT"], view_func=UserApi.as_view("user_api"))
        self.app.add_url_rule("/api/v1/user/<id>", methods=["GET", "DELETE"], view_func=UserDetailApi.as_view("user_detail_api"))

        self.app.add_url_rule("/api/v1/usersRole", methods=["GET", "POST", "PUT"],view_func=UsersRoleApi.as_view("userrole_api"))
        self.app.add_url_rule("/api/v1/usersRole/<id>", methods=["GET", "DELETE"],view_func=UsersRoleDetailApi.as_view("userroles_detail_api"))

        self.app.add_url_rule("/api/v1/rt", methods=["GET", "POST", "PUT"],view_func=RtApi.as_view("rt_api"))
        self.app.add_url_rule("/api/v1/rt/<id>", methods=["GET", "DELETE"],view_func=RtDetailApi.as_view("rt_detail_api"))
        self.app.add_url_rule("/api/v1/zona", methods=["GET", "POST", "PUT"],view_func=ZonaApi.as_view("zona_api"))
        self.app.add_url_rule("/api/v1/zona/<id>", methods=["GET", "DELETE"],view_func=ZonaDetailApi.as_view("zona_detail_api"))
        self.app.add_url_rule("/api/v1/golongan", methods=["GET", "POST", "PUT"],view_func=GolonganApi.as_view("golongan_api"))
        self.app.add_url_rule("/api/v1/golongan/<id>", methods=["GET", "DELETE"],view_func=GolonganDetailApi.as_view("golongan_detail_api"))

        self.app.add_url_rule("/api/v1/rw", methods=["GET", "POST", "PUT"],view_func=RwApi.as_view("rw_api"))
        self.app.add_url_rule("/api/v1/rw/<id>", methods=["GET", "DELETE"],view_func=RwDetailApi.as_view("rw_detail_api"))
        self.app.add_url_rule("/api/v1/pelanggan", methods=["GET", "POST", "PUT"],view_func=PelangganApi.as_view("pelanggan_api"))
        self.app.add_url_rule("/api/v1/pelanggan/<id>", methods=["GET", "DELETE"],view_func=PelangganDetailApi.as_view("pelanggan_detail_api"))

        self.app.add_url_rule("/api/v1/kelurahan", methods=["GET", "POST", "PUT"],view_func=KelurahanApi.as_view("kelurahan_api"))
        self.app.add_url_rule("/api/v1/kelurahan/<id>", methods=["GET", "DELETE"],view_func=KelurahanDetailApi.as_view("kelurahan_detail_api"))


        self.app.add_url_rule("/api/v1/sambung", methods=["GET", "POST", "PUT"],view_func=SambungApi.as_view("sambung_api"))
        self.app.add_url_rule("/api/v1/sambung/<id>", methods=["GET", "DELETE"],view_func=SambungDetailApi.as_view("sambung_detail_api"))
