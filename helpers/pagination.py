import  math

def page_count(page_size,total):
    total_page =  math.ceil(total / page_size)
    return total_page


def abbreviated_pages(page_size,total_rows,page):
    """
    Return a string containing the list of numbers from 1 to `n`, with
    `page` indicated, and abbreviated with ellipses if too long.
    """
    n = page_count(page_size,total_rows)
    pages = []
    # Build set of pages to display
    if n <= 10:
        pages = set(range(1, n + 1))
    else:
        pages = (set(range(1, 4))
                 | set(range(max(1, page - 2), min(page + 3, n + 1)))
                 | set(range(n - 2, n + 1)))

    # Display pages in order with ellipses
    def display():
        last_page = 0
        for p in sorted(pages):
            if p != last_page + 1: yield { "page" : '...', "current":True }
            yield {
                "page" : p,
                "current" : True if p == page else False
            }
            last_page = p

    return list(display())

if __name__ == '__main__':
    pages = abbreviated_pages(100,2)
    print(pages)