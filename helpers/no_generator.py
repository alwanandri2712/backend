
import string
import random

from datetime import datetime

from app import db


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_sprno():
    engine = db.engine
    connection = engine.raw_connection();
    curno = connection.cursor()
    strSQL = "SELECT key_value FROM sys_config WHERE deleted = false and key_name = 'NO_SPRIN_SCKSPT'"
    curno.execute(strSQL)
    recno = curno.fetchone()
    if (recno):
        sprno = recno[0]
    else:
        sprno = 'SP/0/IX/HUK6.6/2019'

    nama_bulan = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII']
    tanggal_now = datetime.now()
    tanggal = str(tanggal_now.day)
    bulan = nama_bulan[tanggal_now.month - 1]
    tahun = str(tanggal_now.year)
    newsprno_value = ''
    if sprno:
        sprarr = sprno.split('/')
        sprarr
        if (len(sprarr) == 5):
            if RepresentsInt(sprarr[1]):
                bulan_spr = sprarr[2]
                tahun_spr = sprarr[4]
                if ((bulan_spr == bulan) and (tahun_spr == tahun)):
                    newsprno = int(sprarr[1]) + 1
                else:
                    newsprno = 1
                newsprno = '000000' + str(newsprno)
                newsprno = newsprno[-4:]
                newsprno_value = sprarr[0] + '/' + newsprno + '/' + bulan + '/' + sprarr[3] + '/' + tahun
                curupd = connection.cursor()
                strSQL = "UPDATE sys_config SET key_value = '" + newsprno_value + "' WHERE key_name = 'NO_SPRIN_SCKSPT' AND deleted = false"
                curupd.execute(strSQL)
                connection.commit()
            else:
                newsprno_value = ''
        else:
            newsprno_value = ''
    else:
        newsprno_value = ''
    return newsprno_value
def random_generator(size=6, chars=string.ascii_lowercase):
    return ''.join(random.choice(chars) for x in range(size))

