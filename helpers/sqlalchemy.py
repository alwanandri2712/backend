from flask import json

from models.entities import *
import string
import uuid
import uuid as uuid_package
from sqlalchemy.dialects.postgresql import UUID as PG_UUID
from sqlalchemy import TypeDecorator, CHAR
from sqlalchemy.ext.declarative import DeclarativeMeta


def get_camelcase(strval, sep='_'):
    if strval.find(sep) == -1:
        return strval[0:1].lower() + strval[1:len(strval)]
    arr_str = string.capwords(strval, sep).split(sep)
    arr_str[0] = arr_str[0].lower()
    return ''.join(arr_str)


def get_pascalcase(strval, sep='_'):
    strval = str(strval)
    if strval.find(sep) == -1:
        fchar = strval[0:1]
        remain_char = strval[1:]
        return fchar.capitalize() + remain_char
    return string.capwords(strval, '_').replace('_', '')

def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        value = getattr(row, column.name)
        if type(value) in [str,uuid.UUID]:
            d[column.name] = str(value)
        else:
            d[column.name] = value

    return d

def filter_query_single(q,f,model_name=""):
    operator = f.get("operator") if f.get("operator") is not None else "=="
    if type(f.get("value")) == str:
        exp = "{0}.{1}{2}'{3}'".format(model_name, f.get("property"), operator, f.get("value"))
    else:
        exp = "{0}.{1}{2}{3}".format(model_name, f.get("property"), operator, f.get("value"))
    if f.get("operator") == "like":
        exp = "{0}.{1}.ilike('%{2}%')".format(model_name, f.get("property"), f.get("value"))
    if f.get("operator") == "in":
        exp = "{0}.{1}.in_({2})".format(model_name, f.get("property") + ".c.", f.get("value"))

    q = q.filter(eval(exp))
    return q

def filter_query(q,filter,model_name=""):
    for f in filter:
        operator = f.get("operator") if f.get("operator") is not None else "=="


        if f.get("operator") == "like":
            if model_name:
                exp = "{0}.{1}.ilike('%{2}%')".format(model_name,f.get("property"), f.get("value"))
            else:
                exp = "{0}.ilike('%{1}%')".format(f.get("property"), f.get("value"))
        elif f.get("operator") == "in":
            values = f.get("value")
            in_clause = "["
            vals = []
            for v in values:
                value = "'" + v + "'"
                vals.append(value)
            in_clause += ",".join(vals) + "]"
            if model_name:
                exp = "{0}.{1}.in_({2})".format(model_name,f.get("property"), in_clause)
            else:
                exp = "{0}.in_({1})".format(f.get("property"), in_clause)
        else:
            if type(f.get("value")) == str:
                if model_name:
                    exp = "{0}.{1}{2}'{3}'".format(model_name, f.get("property"), operator, f.get("value"))
                else:
                    exp = "{0}{1}'{2}'".format(f.get("property"), operator, f.get("value"))
            else:
                if model_name:
                    exp = "{0}.{1}{2}{3}".format(model_name, f.get("property"), operator, f.get("value"))
                else:
                    exp = "{0}{1}{2}".format(f.get("property"), operator, f.get("value"))

        q = q.filter(eval(exp))
    return q


def order_query(q,sorter,model_name):
    for s in sorter:
        exp = ""
        if s.get("dir") == "desc":
            if model_name:
                exp = "{0}.{1}.desc()".format(model_name,s.get("property"))
            else:
                exp = "{0}.desc()".format(s.get("property"))
        else:
            if model_name:
                exp = "{0}.{1}".format(model_name, s.get("property"))
            else:
                exp = "{0}".format(s.get("property"))

        q = q.order_by(eval(exp))
    return q


class GUID(TypeDecorator):
    impl = CHAR

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(PG_UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == 'postgresql':
            return str(value)
        else:
            if not isinstance(value, uuid_package.UUID):
                return "%.32x" % uuid_package.UUID(value)
            else:
                # hexstring
                return "%.32x" % value

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            return uuid_package.UUID(value)



class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)