import base64


def file_to_base64(filepath):
    f = open(filepath, "rb")
    content = f.read()
    encoded_content = base64.b64encode(content)
    f.close()
    return encoded_content