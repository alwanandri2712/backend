
from models.entities import Role
from libs.base_module import ServiceCrudBase


class RoleService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self,Role)