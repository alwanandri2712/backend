
from models.entities import UsersRole, User
from libs.base_module import ServiceCrudBase
from helpers.sqlalchemy import row2dict, filter_query, order_query, filter_query_single


class UsersRoleService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self,UsersRole)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(UsersRole)\
            .filter(UsersRole.id == id)\
            .filter(UsersRole.deleted == False)\
            .first()
        data = {
            "id" : q.id,
            "user_id" : q.user_id,
            "role_id" : q.role_id,
            "role" : q.role.name,
            "user" : q.user.email
        }
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(UsersRole,User.full_name,User.email)\
                        .join(User,User.id == UsersRole.user_id)
        if filter:
            for f in filter:
                if f.get("property") == "full_name":
                    q.filter(User.full_name.ilike('%' + f.get("value") + '%'))
                else:
                    q = filter_query_single(q,f,"UsersRole")
        if sort:
            q = order_query(q, sort, "UsersRole")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        row = UsersRole()
        for row in q:
            row_dict = {
                "id" : row.UsersRole.id,
                "user_id" : row.UsersRole.user_id,
                "role_id" : row.UsersRole.role_id,
                "role" : row.UsersRole.role.name,
                "user_email" : row.email,
                "user_full_name" : row.full_name
            }
            data["rows"].append(row_dict)
        return data
