
from models.entities import User,UsersRole
from libs.base_module import ServiceCrudBase
from helpers.sqlalchemy import row2dict, filter_query, order_query
import uuid

class UserService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self,User)

    def reset_password(self,data):
        user = self.session.query(User)\
              .filter(User.email == data.get("updated_by")).first()
        if user is not None:
            user.password = data.get("new_password")
            user.updated_by = data.get("updated_by")
            user.updated_date = data.get("updated_date")
            self.session.commit()

    def update_password(self,data):
        user = self.session.query(User)\
              .filter(User.email == data.get("updated_by")).first()
        if user is not None:
            user.password = data.get("new_password")
            user.updated_by = data.get("updated_by")
            user.updated_date = data.get("updated_date")
            self.session.commit()

    def create(self, data):
        created = None
        user = User()
        user.username = data.get("username")
        user.email = data.get("email")
        user.deleted = False
        user.email_confirmed_at = data.get("email_confirmed_at")
        user.password = data.get("password")
        user.is_active = data.get("is_active")
        user.first_name = data.get("first_name")
        user.last_name = data.get("last_name")
        user.deleted = data.get("deleted")
        user.created_by = data.get("created_by")
        user.created_date = data.get("created_date")
        self.session.add(user)
        
        #create default role
        userRole = UsersRole()
        userRole.deleted = False
        userRole.user = user
        userRole.role_id = '1223c998-a38f-432f-9f4b-aa142238e518'
        userRole.created_by = user.created_by
        userRole.created_date = user.created_date
        self.session.add(userRole)

        self.session.commit()
        created = user.id
        return created