import datetime
import uuid
from models.entities import Kelurahan
from helpers.sqlalchemy import row2dict, filter_query, order_query
from libs.base_module import ServiceCrudBase


class KelurahanBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Kelurahan) \
                .filter(Kelurahan.id == id) \
                .filter(Kelurahan.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(Kelurahan)
        if filter:
            q = filter_query(q, filter, "Kelurahan")
        if sort:
            q = order_query(q, sort, "Kelurahan")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        kelurahan = Kelurahan()
        kelurahan.id_zona = data.get("id_zona")
        kelurahan.kode_kelurahan = data.get("kode_kelurahan")
        kelurahan.nama_kelurahan = data.get("nama_kelurahan")
        kelurahan.deleted = data.get("deleted")
        kelurahan.created_by = data.get("created_by")
        kelurahan.created_date = data.get("created_date")
        self.session.add(kelurahan)
        self.session.commit()
        created = kelurahan.id
        return created

    def update(self, data):
        kelurahan = self.session.query(Kelurahan).get(data.get("id"))
        if kelurahan is not None:
            kelurahan.id_zona = data.get("id_zona") if data.get("id_zona") else kelurahan.id_zona
            kelurahan.kode_kelurahan = data.get("kode_kelurahan") if data.get("kode_kelurahan") else kelurahan.kode_kelurahan
            kelurahan.nama_kelurahan = data.get("nama_kelurahan") if data.get("nama_kelurahan") else kelurahan.nama_kelurahan
            kelurahan.deleted = data.get("deleted") if data.get("deleted") else kelurahan.deleted
            self.session.commit()

    def delete(self, id, delete_by):
        kelurahan = self.session.query(Kelurahan).get(id)
        if kelurahan is not None:
            kelurahan.deleted = True
            kelurahan.updated_by = delete_by
            kelurahan.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        kelurahan = self.session.query(Kelurahan).get(id)
        if kelurahan is not None:
            kelurahan.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))