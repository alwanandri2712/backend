import datetime
import uuid
from app.models.entities import Role
from app.helpers.sqlalchemy import row2dict, filter_query, order_query
from app.libs.base_module import ServiceCrudBase


class RoleBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Role).get(id)
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        q = self.session.query(Role)
        if filter:
            q = filter_query(q, filter, "Role")
        if sort:
            q = order_query(q, sort, "Role")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        role = Role()
        role.name = data.get("name")
        role.label = data.get("label")
        role.created_by = data.get("created_by")
        role.created_date = data.get("created_date")
        self.session.add(role)
        self.session.commit()
        created = role.id
        return created

    def update(self, data):
        role = self.session.query(Role).get(data.get("id"))
        if role is not None:
            role.name = data.get("name")
            role.label = data.get("label")
            self.session.commit()

    def delete(self, id, delete_by):
        role = self.session.query(Role).get(id)
        if role is not None:
            role.deleted = True
            role.updated_by = delete_by
            role.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        role = self.session.query(Role).get(id)
        if role is not None:
            role.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))