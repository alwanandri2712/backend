import datetime
import uuid
from models.entities import Rw
from helpers.sqlalchemy import row2dict, filter_query, order_query
from libs.base_module import ServiceCrudBase


class RwBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Rw) \
                .filter(Rw.id == id) \
                .filter(Rw.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(Rw)
        if filter:
            q = filter_query(q, filter, "Rw")
        if sort:
            q = order_query(q, sort, "Rw")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        rw = Rw()
        rw.kode_rw = data.get("kode_rw")
        rw.nama_rw = data.get("nama_rw")
        rw.keterangan = data.get("keterangan")
        rw.deleted = data.get("deleted")
        rw.created_by = data.get("created_by")
        rw.created_date = data.get("created_date")
        self.session.add(rw)
        self.session.commit()
        created = rw.id
        return created

    def update(self, data):
        rw = self.session.query(Rw).get(data.get("id"))
        if rw is not None:
            rw.kode_rw = data.get("kode_rw") if data.get("kode_rw") else rw.kode_rw
            rw.nama_rw = data.get("nama_rw") if data.get("nama_rw") else rw.nama_rw
            rw.keterangan = data.get("keterangan") if data.get("keterangan") else rw.keterangan
            rw.deleted = data.get("deleted") if data.get("deleted") else rw.deleted
            self.session.commit()

    def delete(self, id, delete_by):
        rw = self.session.query(Rw).get(id)
        if rw is not None:
            rw.deleted = True
            rw.updated_by = delete_by
            rw.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        rw = self.session.query(Rw).get(id)
        if rw is not None:
            rw.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))