import datetime
import uuid
from models.entities import Sambung
from helpers.sqlalchemy import row2dict, filter_query, order_query
from libs.base_module import ServiceCrudBase


class SambungBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Sambung) \
                .filter(Sambung.id == id) \
                .filter(Sambung.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(Sambung)
        if filter:
            q = filter_query(q, filter, "Sambung")
        if sort:
            q = order_query(q, sort, "Sambung")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        sambung = Sambung()
        sambung.id_pelanggan = data.get("id_pelanggan")
        sambung.id_golongan = data.get("id_golongan")
        sambung.id_zona = data.get("id_zona")
        sambung.id_kelurahan = data.get("id_kelurahan")
        sambung.id_rw = data.get("id_rw")
        sambung.id_rt = data.get("id_rt")
        sambung.no_rumah = data.get("no_rumah")
        sambung.no_sambung = data.get("no_sambung")
        sambung.tanggal_daftar = data.get("tanggal_daftar")
        sambung.tanggal_pasang = data.get("tanggal_pasang")
        sambung.bulan_pasang = data.get("bulan_pasang")
        sambung.tahun_pasang = data.get("tahun_pasang")
        sambung.latitude = data.get("latitude")
        sambung.longitude = data.get("longitude")
        sambung.status = data.get("status")
        sambung.deleted = data.get("deleted")
        sambung.created_by = data.get("created_by")
        sambung.created_date = data.get("created_date")
        self.session.add(sambung)
        self.session.commit()
        created = sambung.id
        return created

    def update(self, data):
        sambung = self.session.query(Sambung).get(data.get("id"))
        if sambung is not None:
            sambung.id_pelanggan = data.get("id_pelanggan") if data.get("id_pelanggan") else sambung.id_pelanggan
            sambung.id_golongan = data.get("id_golongan") if data.get("id_golongan") else sambung.id_golongan
            sambung.id_zona = data.get("id_zona") if data.get("id_zona") else sambung.id_zona
            sambung.id_kelurahan = data.get("id_kelurahan") if data.get("id_kelurahan") else sambung.id_kelurahan
            sambung.id_rw = data.get("id_rw") if data.get("id_rw") else sambung.id_rw
            sambung.id_rt = data.get("id_rt") if data.get("id_rt") else sambung.id_rt
            sambung.no_rumah = data.get("no_rumah") if data.get("no_rumah") else sambung.no_rumah
            sambung.no_sambung = data.get("no_sambung") if data.get("no_sambung") else sambung.no_sambung
            sambung.tanggal_daftar = data.get("tanggal_daftar") if data.get("tanggal_daftar") else sambung.tanggal_daftar
            sambung.tanggal_pasang = data.get("tanggal_pasang") if data.get("tanggal_pasang") else sambung.tanggal_pasang
            sambung.bulan_pasang = data.get("bulan_pasang") if data.get("bulan_pasang") else sambung.bulan_pasang
            sambung.tahun_pasang = data.get("tahun_pasang") if data.get("tahun_pasang") else sambung.tahun_pasang
            sambung.latitude = data.get("latitude") if data.get("latitude") else sambung.latitude
            sambung.longitude = data.get("longitude") if data.get("longitude") else sambung.longitude
            sambung.status = data.get("status") if data.get("status") else sambung.status
            sambung.deleted = data.get("deleted") if data.get("deleted") else sambung.deleted
            self.session.commit()

    def delete(self, id, delete_by):
        sambung = self.session.query(Sambung).get(id)
        if sambung is not None:
            sambung.deleted = True
            sambung.updated_by = delete_by
            sambung.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        sambung = self.session.query(Sambung).get(id)
        if sambung is not None:
            sambung.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))