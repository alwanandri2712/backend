import datetime
import uuid
from models.entities import Pelanggan
from helpers.sqlalchemy import row2dict, filter_query, order_query
from libs.base_module import ServiceCrudBase


class PelangganBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Pelanggan) \
                .filter(Pelanggan.id == id) \
                .filter(Pelanggan.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(Pelanggan)
        if filter:
            q = filter_query(q, filter, "Pelanggan")
        if sort:
            q = order_query(q, sort, "Pelanggan")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        pelanggan = Pelanggan()
        pelanggan.kode_pelanggan = data.get("kode_pelanggan")
        pelanggan.nama_pelanggan = data.get("nama_pelanggan")
        pelanggan.alamat = data.get("alamat")
        pelanggan.telp = data.get("telp")
        pelanggan.mobile = data.get("mobile")
        pelanggan.fax = data.get("fax")
        pelanggan.no_ktp = data.get("no_ktp")
        pelanggan.email = data.get("email")
        pelanggan.status = data.get("status")
        pelanggan.deleted = data.get("deleted")
        pelanggan.created_by = data.get("created_by")
        pelanggan.created_date = data.get("created_date")
        self.session.add(pelanggan)
        self.session.commit()
        created = pelanggan.id
        return created

    def update(self, data):
        pelanggan = self.session.query(Pelanggan).get(data.get("id"))
        if pelanggan is not None:
            pelanggan.kode_pelanggan = data.get("kode_pelanggan") if data.get("kode_pelanggan") else pelanggan.kode_pelanggan
            pelanggan.nama_pelanggan = data.get("nama_pelanggan") if data.get("nama_pelanggan") else pelanggan.nama_pelanggan
            pelanggan.alamat = data.get("alamat") if data.get("alamat") else pelanggan.alamat
            pelanggan.telp = data.get("telp") if data.get("telp") else pelanggan.telp
            pelanggan.mobile = data.get("mobile") if data.get("mobile") else pelanggan.mobile
            pelanggan.fax = data.get("fax") if data.get("fax") else pelanggan.fax
            pelanggan.no_ktp = data.get("no_ktp") if data.get("no_ktp") else pelanggan.no_ktp
            pelanggan.email = data.get("email") if data.get("email") else pelanggan.email
            pelanggan.status = data.get("status") if data.get("status") else pelanggan.status
            pelanggan.deleted = data.get("deleted") if data.get("deleted") else pelanggan.deleted
            self.session.commit()

    def delete(self, id, delete_by):
        pelanggan = self.session.query(Pelanggan).get(id)
        if pelanggan is not None:
            pelanggan.deleted = True
            pelanggan.updated_by = delete_by
            pelanggan.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        pelanggan = self.session.query(Pelanggan).get(id)
        if pelanggan is not None:
            pelanggan.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))