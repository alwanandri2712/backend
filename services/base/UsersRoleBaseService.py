import datetime
import uuid
from app.models.entities import UsersRole
from app.helpers.sqlalchemy import row2dict, filter_query, order_query
from app.libs.base_module import ServiceCrudBase


class UsersRoleBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(UsersRole).get(id)
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        q = self.session.query(UsersRole)
        if filter:
            q = filter_query(q, filter, "UsersRole")
        if sort:
            q = order_query(q, sort, "UsersRole")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        usersRole = UsersRole()
        usersRole.user_id = data.get("userId")
        usersRole.role_id = data.get("roleId")
        usersRole.deleted = data.get("deleted")
        usersRole.created_by = data.get("created_by")
        usersRole.created_date = data.get("created_date")
        self.session.add(usersRole)
        self.session.commit()
        created = usersRole.id
        return created

    def update(self, data):
        usersRole = self.session.query(UsersRole).get(data.get("id"))
        if usersRole is not None:
            usersRole.user_id = data.get("userId")
            usersRole.role_id = data.get("roleId")
            usersRole.deleted = data.get("deleted")
            self.session.commit()

    def delete(self, id, delete_by):
        usersRole = self.session.query(UsersRole).get(id)
        if usersRole is not None:
            usersRole.deleted = True
            usersRole.updated_by = delete_by
            usersRole.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        usersRole = self.session.query(UsersRole).get(id)
        if usersRole is not None:
            usersRole.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))