import datetime
import uuid
from models.entities import Zona
from helpers.sqlalchemy import row2dict, filter_query, order_query
from libs.base_module import ServiceCrudBase


class ZonaBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Zona) \
                .filter(Zona.id == id) \
                .filter(Zona.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(Zona)
        if filter:
            q = filter_query(q, filter, "Zona")
        if sort:
            q = order_query(q, sort, "Zona")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        zona = Zona()
        zona.kode_zona = data.get("kode_zona")
        zona.nama_zona = data.get("nama_zona")
        zona.keterangan = data.get("keterangan")
        zona.update_by = data.get("update_by")
        zona.update_date = data.get("update_date")
        zona.deleted = data.get("deleted")
        zona.created_by = data.get("created_by")
        zona.created_date = data.get("created_date")
        self.session.add(zona)
        self.session.commit()
        created = zona.id
        return created

    def update(self, data):
        zona = self.session.query(Zona).get(data.get("id"))
        if zona is not None:
            zona.kode_zona = data.get("kode_zona") if data.get("kode_zona") else zona.kode_zona
            zona.nama_zona = data.get("nama_zona") if data.get("nama_zona") else zona.nama_zona
            zona.keterangan = data.get("keterangan") if data.get("keterangan") else zona.keterangan
            zona.update_by = data.get("update_by") if data.get("update_by") else zona.update_by
            zona.update_date = data.get("update_date") if data.get("update_date") else zona.update_date
            zona.deleted = data.get("deleted") if data.get("deleted") else zona.deleted
            self.session.commit()

    def delete(self, id, delete_by):
        zona = self.session.query(Zona).get(id)
        if zona is not None:
            zona.deleted = True
            zona.updated_by = delete_by
            zona.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        zona = self.session.query(Zona).get(id)
        if zona is not None:
            zona.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))