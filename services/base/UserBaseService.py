import datetime
import uuid
from app.models.entities import User
from app.helpers.sqlalchemy import row2dict, filter_query, order_query
from app.libs.base_module import ServiceCrudBase


class UserBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(User) \
                .filter(User.id == id) \
                .filter(User.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(User)
        if filter:
            q = filter_query(q, filter, "User")
        if sort:
            q = order_query(q, sort, "User")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        user = User()
        user.email = data.get("email")
        user.email_confirmed_at = data.get("email_confirmed_at")
        user.password = data.get("password")
        user.is_active = data.get("is_active")
        user.first_name = data.get("first_name")
        user.last_name = data.get("last_name")
        user.deleted = data.get("deleted")
        user.created_by = data.get("created_by")
        user.created_date = data.get("created_date")
        self.session.add(user)
        self.session.commit()
        created = user.id
        return created

    def update(self, data):
        user = self.session.query(User).get(data.get("id"))
        if user is not None:
            user.email = data.get("email") if data.get("email") else user.email
            user.email_confirmed_at = data.get("email_confirmed_at") if data.get("email_confirmed_at") else user.email_confirmed_at
            user.password = data.get("password") if data.get("password") else user.password
            user.is_active = data.get("is_active") if data.get("is_active") else user.is_active
            user.first_name = data.get("first_name") if data.get("first_name") else user.first_name
            user.last_name = data.get("last_name") if data.get("last_name") else user.last_name
            user.deleted = data.get("deleted") if data.get("deleted") else user.deleted
            self.session.commit()

    def delete(self, id, delete_by):
        user = self.session.query(User).get(id)
        if user is not None:
            user.deleted = True
            user.updated_by = delete_by
            user.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        user = self.session.query(User).get(id)
        if user is not None:
            user.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))