import datetime
import uuid
from models.entities import Rt
from helpers.sqlalchemy import row2dict, filter_query, order_query
from libs.base_module import ServiceCrudBase


class RtBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Rt) \
                .filter(Rt.id == id) \
                .filter(Rt.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(Rt)
        if filter:
            q = filter_query(q, filter, "Rt")
        if sort:
            q = order_query(q, sort, "Rt")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        rt = Rt()
        rt.kode_rt = data.get("kode_rt")
        rt.nama_rt = data.get("nama_rt")
        rt.keterangan = data.get("keterangan")
        rt.created_by = data.get("created_by")
        rt.created_date = data.get("created_date")
        self.session.add(rt)
        self.session.commit()
        created = rt.id
        return created

    def update(self, data):
        rt = self.session.query(Rt).get(data.get("id"))
        if rt is not None:
            rt.kode_rt = data.get("kode_rt") if data.get("kode_rt") else rt.kode_rt
            rt.nama_rt = data.get("nama_rt") if data.get("nama_rt") else rt.nama_rt
            rt.keterangan = data.get("keterangan") if data.get("keterangan") else rt.keterangan
            self.session.commit()

    def delete(self, id, delete_by):
        rt = self.session.query(Rt).get(id)
        if rt is not None:
            rt.deleted = True
            rt.updated_by = delete_by
            rt.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        rt = self.session.query(Rt).get(id)
        if rt is not None:
            rt.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))