import datetime
import uuid
from models.entities import Golongan
from helpers.sqlalchemy import row2dict, filter_query, order_query
from libs.base_module import ServiceCrudBase


class GolonganBaseService(ServiceCrudBase):

    def __init__(self):
        ServiceCrudBase.__init__(self)

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(Golongan) \
                .filter(Golongan.id == id) \
                .filter(Golongan.deleted == False) \
                .first()
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        filter.append({
            "property": "deleted",
            "value": False
        })
        q = self.session.query(Golongan)
        if filter:
            q = filter_query(q, filter, "Golongan")
        if sort:
            q = order_query(q, sort, "Golongan")

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        created = None
        golongan = Golongan()
        golongan.kode_golongan = data.get("kode_golongan")
        golongan.nama_golongan = data.get("nama_golongan")
        golongan.keterangan = data.get("keterangan")
        golongan.deleted = data.get("deleted")
        golongan.created_by = data.get("created_by")
        golongan.created_date = data.get("created_date")
        self.session.add(golongan)
        self.session.commit()
        created = golongan.id
        return created

    def update(self, data):
        golongan = self.session.query(Golongan).get(data.get("id"))
        if golongan is not None:
            golongan.kode_golongan = data.get("kode_golongan") if data.get("kode_golongan") else golongan.kode_golongan
            golongan.nama_golongan = data.get("nama_golongan") if data.get("nama_golongan") else golongan.nama_golongan
            golongan.keterangan = data.get("keterangan") if data.get("keterangan") else golongan.keterangan
            golongan.deleted = data.get("deleted") if data.get("deleted") else golongan.deleted
            self.session.commit()

    def delete(self, id, delete_by):
        golongan = self.session.query(Golongan).get(id)
        if golongan is not None:
            golongan.deleted = True
            golongan.updated_by = delete_by
            golongan.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        golongan = self.session.query(Golongan).get(id)
        if golongan is not None:
            golongan.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))