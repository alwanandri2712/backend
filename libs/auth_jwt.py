from functools import wraps
from flask.json import jsonify
from flask_jwt_extended import verify_jwt_in_request, get_jwt_claims

# Here is a custom decorator that verifies the JWT is present in
# the request, as well as insuring that this user has a role of
# `admin` in the access token
def authorize_roles(roles):
    if type(roles) is str:
        if roles.find(",") != -1:
            roles = roles.split(",")
        else:
            roles = [roles]
    def _authorize_roles(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt_claims()

            def is_in_role():
                claim_roles = claims["roles"]
                if "admin" in claim_roles:  # give al access to admin
                    return fn(*args, **kwargs)
                for claim_role in claim_roles:
                    role = str.strip(claim_role)
                    if role in roles:
                        return fn(*args, **kwargs)
                return jsonify(msg='Access to this resource was denied.'), 403

            return is_in_role()
        return wrapper
    return _authorize_roles

def get_roles(user):
    claim_roles = []
    if user is not None:
        roles = user.role();
        for role in roles:
            claim_roles.append(role)
    return claim_roles


