
from app import db
import flask_jwt_extended
from flask.views import MethodView
from flask import request,jsonify
import simplejson
import logging
from helpers.sqlalchemy import row2dict, filter_query, order_query
import datetime
from helpers.pagination import abbreviated_pages
from libs.auth_jwt import authorize_roles

PAGE_SIZE = 20

logger = logging.getLogger('werkzeug')

class ServiceCrudBase:

    def __init__(self,entity_class):
        self.session = db.session
        self.default_page_size = PAGE_SIZE
        self.entity = entity_class

    def find_by_pk(self,id):
        data = {}
        q = self.session.query(self.entity).get(id)
        data = row2dict(q)
        return data

    def find(self, filter, sort, page=1, page_size=20):
        data = {
            "rows" : [],
            "count" : 0
        }
        q = self.session.query(self.entity)
        filter.append({
            "property" : "deleted",
            "value" : False
        })
        if filter:
            q = filter_query(q, filter, self.entity.__name__)
        if sort:
            q = order_query(q, sort, self.entity.__name__)

        data["count"] = q.count()
        q = q.limit(page_size).offset((page - 1) * page_size)
        data["rows"] = [row2dict(row) for row in q]
        return data

    def create(self, data):
        entity = self.entity()
        for c in self.entity.__table__.columns:
            if c.description not in ["updated_date","updated_by","deleted"] and c.description != "id":
                exec("entity.{0}=data.get('{0}') if data.get('{0}') else None".format(c.description))
        entity.deleted = False
        self.session.add(entity)
        self.session.commit()
        return entity

    def update(self, data):
        entity = self.session.query(self.entity).get(data.get("id"))
        if entity is not None:
            for c in self.entity.__table__.columns:
                if c.description not in ["created_date", "created_by","deleted"] and c.description not in entity.__table__.primary_key.columns:
                    exec("entity.{0}=data.get('{0}') if data.get('{0}') else entity.{0}".format(c.description))
            self.session.commit()

    def delete(self, id, delete_by):
        entity = self.session.query(self.entity).get(id)
        if entity is not None:
            entity.deleted = True
            entity.updated_by = delete_by
            entity.updated_date = datetime.datetime.now()
            self.session.commit()

    def true_delete(self, id):
        entity = self.session.query(self.entity).get(id)
        if entity is not None:
            entity.delete()
            self.session.commit()
        else:
            raise Exception("Data not found for id {0}".format(id))


class HandlerBase(MethodView):

    def __init__(self):
        self.default_page_size = PAGE_SIZE

    def get_current_user(self):
        return flask_jwt_extended.get_jwt_identity()

    def get_current_roles(self):
        claims = flask_jwt_extended.get_jwt_claims()
        return claims.get("roles")

    def write_log(self,e):
        logger.exception(e)


class HandleBaseEntityMixin(HandlerBase):

    def __init__(self,service,form_class):
        self.service = service
        self.form = form_class
        HandlerBase.__init__(self)

    @authorize_roles("user")
    def get(self):
        try:
            success = True
            rows = []
            count = 0
            message = ""
            strfilter = request.args.get("filter")
            strsorter = request.args.get("sort")
            page = int(request.args.get("page")) if request.args.get("page") is not None else 1
            page_size = int(request.args.get("page_size")) if request.args.get("page_size") else self.default_page_size
            filter = []
            sort = []
            if strfilter:
                filter = simplejson.loads(strfilter)
            if strsorter:
                sort = simplejson.loads(strsorter)
            page_numbers = []
            try:
                res = self.service.find(filter, sort, page, page_size)
                rows = res.get("rows")
                count = res.get("count")
                page_numbers = abbreviated_pages(page_size,count,page)
            except Exception as e:
                success = False
                message = "Sistem Error, Silahkan kontak IT Support. Error: " + str(e)
                self.write_log(e)

            ret = jsonify(success=success, rows=rows, total=count, message=message,page_numbers=page_numbers)
            return ret, 200
        except Exception as e:
            ret = jsonify(success=False, message=str(e))
            return ret, 200

    @authorize_roles("user")
    def post(self):
        try:
            success = True
            message = "Simpan Data Berhasil"
            model_form = self.form()
            id=None
            data = model_form.data
            if not model_form.validate_on_submit():
                success = False
                message = "Form tidak valid"
            else:
                try:
                    data["created_date"] = datetime.datetime.now()
                    data["created_by"] = self.get_current_user()
                    created = self.service.create(data)
                    data["id"] = created.id;
                except Exception as e:
                    success = False
                    self.write_log(e)
                    message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)

            ret = dict(message=message, success=success, data=data, errors=model_form.errors)
            return jsonify(ret), 200
        except Exception as e:
            ret = jsonify(success=False, message=str(e))
            return ret, 200

    @authorize_roles("user")
    def put(self):
        try:
            success = True
            message = "Ubah Data Berhasil"
            model_form = self.form()

            data = model_form.data
            if not model_form.validate_on_submit():
                success = False
                message = "Form tidak valid"
            else:
                try:
                    data["updated_date"] = datetime.datetime.now()
                    data["updated_by"] = self.get_current_user()
                    self.service.update(data)
                except Exception as e:
                    success = False
                    self.write_log(e)
                    message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)

            ret = dict(message=message, success=success, data=data, errors=model_form.errors)
            return jsonify(ret), 200
        except Exception as e:
            ret = jsonify(success=False, message=str(e))
            return ret, 200


class HandleBaseEntityDetailMixin(HandlerBase):

    def __init__(self,service,form_class):
        self.service = service
        self.form = form_class

    @authorize_roles("user")
    def get(self, id):
        try:
            success = True
            data = {}
            count = 0
            message = ""
            try:
                data = self.service.find_by_pk(id)
            except Exception as e:
                success = False
                message = str(e)
                message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)
                self.write_log(e)
            ret = jsonify(success=success, data=data, count=count, message=message)
            return ret, 200
        except Exception as e:
            ret = jsonify(success=False, message=str(e))
            return ret, 200

    @authorize_roles("user")
    def delete(self,id):
        try:
            success = True
            message = "Hapus Data Berhasil"
            try:
                self.service.delete(id, self.get_current_user())
            except Exception as e:
                success = False
                self.write_log(e)
                message = "Sistem Error, Silahkan Hubungi IT Support. Error: " + str(e)

            ret = dict(message=message, success=success)
            return jsonify(ret), 200
        except Exception as e:
            ret = jsonify(success=False, message=str(e))
            return ret, 200